package tests;

import static org.junit.Assert.*;


import org.junit.Test;
import projet.Plateau;
import projet.Projet_Exception;
/**
 * Creation de la classe de tests pour la classe Plateau
 */
public class TestPlateau {
	/**
	 * test du constructeur de Plateau
	 * @throws Projet_Exception
	 */
	@Test
	public void testConstructeur() throws Projet_Exception {
		Plateau pl = new Plateau();
		
		assertEquals("Le plateau devrait etre initialise", 21, pl.getTuile().length);
	}

}
