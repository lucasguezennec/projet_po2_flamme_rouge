package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import projet.Cycliste;
import projet.Jeu;
import projet.Joueur;
import projet.Plateau;
import projet.Projet_Exception;
import projet.Rouleur;
import projet.Sprinteur;
import projet.Tuile;
/**
 * Creation de la classe de tests pour la classe Jeu
 */
public class TestJeu {
	/**
	 * test du constructeur de Jeu
	 * @throws Projet_Exception
	 */
	@Test
	public void testConstructeur() throws Projet_Exception {
		Plateau p = new Plateau();
		Joueur[] jo = new Joueur[4];
		ArrayList<Cycliste> c = new ArrayList<Cycliste>();
		jo[0]= new Joueur("Moulard ","Jaune",1,new Rouleur(1,0,1,0,p.getTuile()[1]), new Sprinteur());
		jo[1]= new Joueur("Mouloud","Bleu",2,new Rouleur(2,0,2,0,p.getTuile()[1]), new Sprinteur());
		jo[2]= new Joueur("Arbertu","Blanc",3,new Rouleur(3,1,2,0,p.getTuile()[1]), new Sprinteur());
		jo[3]= new Joueur("Boulet","Rouge",4,new Rouleur(4,0,4,0,p.getTuile()[1]), new Sprinteur());


		Jeu j = new Jeu(4,jo,p);
		
		assertEquals("Il devrait y avoir 4 joueurs",4,jo.length);
		assertEquals("Le plateau devrait etre initialise",j.getPlateau(),p);
	}

}
