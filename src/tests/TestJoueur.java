package tests;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import projet.Joueur;
import projet.Projet_Exception;
import projet.Rouleur;
import projet.Sprinteur;
import projet.Tuile;
/**
 * Creation de la classe de tests pour la classe Joueur
 */
public class TestJoueur {
	/**
	 * test du constructeur de Joueur
	 * @throws Projet_Exception
	 */
	@Test
	public void testConstructeurParam() throws Projet_Exception {
		Tuile t = new Tuile(1, 7);
		Tuile tp = new Tuile(2, 8);
		Rouleur r = new Rouleur(3,2,3,5,tp);
		Sprinteur s = new Sprinteur(3,1,1,5,t);
		Joueur j = new Joueur("Moulard","Jaune",3,r,s);
		
		assertEquals("le nom devrait etre Moulard", "Moulard", j.getNom());
		assertEquals("la couleur devrait etre jaune", "Jaune", j.getCouleur());
		assertEquals("le num�ro devrait etre 3", 3, j.getNumero());
		assertEquals("le joueur devrait avoir un rouleur", r, j.getRouleur());
		assertEquals("le joueur devrait avoir un sprinteur", s, j.getSprinteur());
	}
}
