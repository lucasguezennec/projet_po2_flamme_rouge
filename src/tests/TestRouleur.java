package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import projet.Plateau;
import projet.Projet_Exception;
import projet.Rouleur;
import projet.Sprinteur;
import projet.Tuile;

/**
 * Creation de la classe de tests pour la classe Rouleur
 */
public class TestRouleur {
	/**
	 * test du constructeur avec 5 parametres de Rouleur
	 * @throws Projet_Exception
	 */
	@Test
	public void testConstructeurSuper() throws Projet_Exception {
		Tuile t = new Tuile(2,5);
		Rouleur r = new Rouleur(3,1,3,5,t);
		
		assertEquals("Le numero du joueur devrait etre 3", 3, r.getNumJoueur());
		assertEquals("Le numero de sa file devrait etre 1", 1, r.getFileC());
		assertEquals("La positon devrait etre 3", 3, r.getPos());
		assertEquals("La vitesse devrait etre 5", 5, r.getVitesse());
		assertEquals("Le rouleur devrait etre pr�sent sur la tuile", t, r.getTuilePresent());
	}
	/**
	 * test du constructeur avec un parametre Rouleur
	 * @throws Projet_Exception
	 */
	@Test
	public void testConstructeurSprinteur() throws Projet_Exception {
		Tuile t = new Tuile(2,5);
		Rouleur r = new Rouleur(3,1,3,5,t);
		Rouleur rp = new Rouleur(r);
		
		assertEquals("Le numero du joueur devrait etre 3", 3, rp.getNumJoueur());
		assertEquals("Le numero de sa file devrait etre 1", 1, rp.getFileC());
		assertEquals("La positon devrait etre 3", 3, rp.getPos());
		assertEquals("La vitesse devrait etre 5", 5, rp.getVitesse());
		assertEquals("Le rouleur devrait etre pr�sent sur la tuile", t, rp.getTuilePresent());
	}
	/**
	 * test de la methode avancer de Rouleur
	 * @throws Projet_Exception
	 */
	@Test
	public void testAvancerUneCase() throws Projet_Exception {
		Plateau pl = new Plateau();
		Tuile t = new Tuile(2,5);
		Rouleur r = new Rouleur(3,1,3,5,t);
		r.avancer(pl);
		assertEquals("Le rouleur devrait etre sur la case 4", 4, r.getPos());
	}
	/**
	 * test de la methode de changement de file de Rouleur
	 * @throws Projet_Exception
	 */
	@Test
	public void testChangementFile() throws Projet_Exception {
		Plateau pl = new Plateau();
		Tuile t = new Tuile(2,5);
		Rouleur r = new Rouleur(3,1,3,5,t);
		r.changementFile(pl);
		
		assertEquals("Le rouleur drvrait avoir change de file", 0, r.getFileC());
	}
	/**
	 * test de la methode copierRouleur de Rouleur 
	 * @throws Projet_Exception
	 */
	@Test
	public void testCopierRouleur() throws Projet_Exception {
		Tuile t = new Tuile(2,5);
		Rouleur r = new Rouleur(3,1,3,5,t);
		Rouleur res = r.copierRouleur();
		
		assertEquals("Le numero du joueur devrait etre 3", 3, res.getNumJoueur());
		assertEquals("Le numero de sa file devrait etre 1", 1, res.getFileC());
		assertEquals("La positon devrait etre 3", 3, res.getPos());
		assertEquals("La vitesse devrait etre 5", 5, res.getVitesse());
		assertEquals("Le rouleur devrait etre pr�sent sur la tuile", t, res.getTuilePresent());
	}
}
