package tests;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import projet.Carte;
import projet.PiocheRouleur;
import projet.PiocheSprinteur;
/**
 * Creation de la classe de tests pour la classe PiocheSprinteur
 */
public class TestPiocheSprinteur {
	/**
	 * test du constructeur de la classe PiocheSprinteur
	 */
	@Test
	public void testConstructeur() {
		PiocheSprinteur pS = new PiocheSprinteur();
		ArrayList<Carte> c = new ArrayList<Carte>();
		c = pS.getPiocheSprinteur();
		
		assertEquals("Le pioche est remplie", c, pS.getPiocheSprinteur());
	}
	/**
	 * test de la methode pour tirer une carte
	 */
	@Test
	public void testTirerCarte() {		
		PiocheRouleur pR = new PiocheRouleur();
		ArrayList<Carte> c = pR.tirerCarte();
		
		assertEquals("Le joueur devrait avoir 4 cartes", 4, c.size());
	}
	
	@Test
	public void testTirageAleaProba() {
		
	}
	
	@Test
	public void testSwapPiocheDefausse() {
		
	}
	
	@Test
	public void testRetirerCarte() {
		
	}
}
