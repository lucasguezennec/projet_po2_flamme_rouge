package tests;

import static org.junit.Assert.*;

import org.junit.Test;
import projet.Pioche;
import projet.PiocheRouleur;
import projet.PiocheSprinteur;
/**
 * Creation de la classe de tests pour la classe Pioche
 */
public class TestPioche {
	/**
	 * test du constructeur de Pioche
	 */
	@Test
	public void testConstructeur() {
		PiocheRouleur pR = new PiocheRouleur();
		PiocheSprinteur pS = new PiocheSprinteur();
		Pioche p = new Pioche();
		
		assertEquals("il devrait y avoir une pioche Rouleur", pR, p.getPiRouleur());
		assertEquals("il devrait y avoir une pioche Sprinteur", pS, p.getPiSprinteur());
	}
	
	
	@Test
	public void testSwapPiocheNbCartesRouleur() {

	}
}
