package tests;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import projet.PiocheRouleur;
import projet.Carte;
/**
 * Creation de la classe de tests pour la classe PiocheRouleur
 */
public class TestPiocheRouleur {
	/**
	 * test du constructeur de PiocheRouleur
	 */
	@Test
	public void testConstructeur() {
		PiocheRouleur pR = new PiocheRouleur();
		ArrayList<Carte> c = new ArrayList<Carte>();
		c = pR.getPiocheRouleur();
		
		assertEquals("Le pioche est remplie", c, pR.getPiocheRouleur());
	}
	/**
	 * test de la methode pour tirer les cartes 
	 */
	@Test
	public void testTirerCarte() {
		PiocheRouleur pR = new PiocheRouleur();
		ArrayList<Carte> c = pR.tirerCarte();
		
		assertEquals("Le joueur devrait avoir 4 cartes", 4, c.size());
	}
	
	@Test
	public void testTirageAleaProba() {
		
	}
	
	@Test
	public void testSwapPiocheDefausse() {
		
	}
	
	@Test
	public void testRetirerCarte() {
		
	}
}
