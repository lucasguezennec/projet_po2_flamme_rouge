package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import projet.Cycliste;
import projet.Plateau;
import projet.Projet_Exception;
import projet.Rouleur;
import projet.Tuile;
/**
 * Creation de classe de tests pour la classe Tuile
 */
public class TestTuile {
	/**
	 * test du constructeur pour une ligne droite
	 * @throws Projet_Exception
	 */
	@Test
	public void testConstructeurLD() throws Projet_Exception {
		Tuile t = new Tuile(2, 4);
		
		assertEquals("La tuile est une ligne droite", 2, t.getTypeTuile());
		assertEquals("La tuile est la 4eme tuile", 4, t.getPositionSurPlateau());
	}
	/**
	 * test du constructeur pour un virage a droite
	 * @throws Projet_Exception
	 */
	@Test
	public void testConstructeurVD() throws Projet_Exception {
		Tuile t = new Tuile(1, 4);
		
		assertEquals("La tuile est un virage a droite", 1, t.getTypeTuile());
		assertEquals("La tuile est la 4eme tuile", 4, t.getPositionSurPlateau());
	}
	/**
	 * test du constructeur pour un virage a gauche
	 * @throws Projet_Exception
	 */
	@Test
	public void testConstructeurVG() throws Projet_Exception {
		Tuile t = new Tuile(0, 4);
		
		assertEquals("La tuile est un virage a gauche", 0, t.getTypeTuile());
		assertEquals("La tuile est la 4eme tuile", 4, t.getPositionSurPlateau());
	}
	
	@Test
	public void testPresenceCycliste() throws Projet_Exception {
		Tuile t = new Tuile(2,4);
		Rouleur r = new Rouleur(3,0,3,2,t);
		int res = t.presenceCycliste();
		
		assertEquals("La tuile devrait contenir au moins un cycliste", 1, res);
	}

}
