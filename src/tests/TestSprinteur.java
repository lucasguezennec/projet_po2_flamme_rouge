package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import projet.Plateau;
import projet.Projet_Exception;
import projet.Rouleur;
import projet.Sprinteur;
import projet.Tuile;
/**
 * Creation de la classe de tests pour la classe Sprinteur
 */
public class TestSprinteur {
	/**
	 * test du constructeur avec 5 parametres de Sprinteur
	 * @throws Projet_Exception
	 */
	@Test
	public void testConstructeurSuper() throws Projet_Exception {
		Tuile t = new Tuile(2,5);
		Sprinteur s = new Sprinteur(3,1,3,5,t);
		
		assertEquals("Le numero du joueur devrait etre 3", 3, s.getNumJoueur());
		assertEquals("Le numero de sa file devrait etre 1", 1, s.getFileC());
		assertEquals("La positon devrait etre 3", 3, s.getPos());
		assertEquals("La vitesse devrait etre 5", 5, s.getVitesse());
		assertEquals("Le sprinteur devrait etre pr�sent sur la tuile", t, s.getTuilePresent());
	}
	/**
	 * test du constructeur avec un parametre Sprinteur
	 * @throws Projet_Exception
	 */
	@Test
	public void testConstructeurSprinteur() throws Projet_Exception {
		Tuile t = new Tuile(2,5);
		Sprinteur s = new Sprinteur(3,1,3,5,t);
		Sprinteur sp = new Sprinteur(s);
		
		assertEquals("Le numero du joueur devrait etre 3", 3, sp.getNumJoueur());
		assertEquals("Le numero de sa file devrait etre 1", 1, sp.getFileC());
		assertEquals("La positon devrait etre 3", 3, sp.getPos());
		assertEquals("La vitesse devrait etre 5", 5, sp.getVitesse());
		assertEquals("Le sprinteur devrait etre pr�sent sur la tuile", t, sp.getTuilePresent());
	}
	/**
	 * test de la methode avancer de Sprinteur
	 * @throws Projet_Exception
	 */
	@Test
	public void testAvancerUneCase() throws Projet_Exception {
		Plateau pl = new Plateau();
		Tuile t = new Tuile(2,5);
		Sprinteur s = new Sprinteur(3,1,3,5,t);
		s.avancer(pl);
		assertEquals("Le sprinteur devrait etre sur la case 4", 4, s.getPos());
	}
	/**
	 * test de la methode du changement de file de Sprinteur
	 * @throws Projet_Exception
	 */
	@Test
	public void testChangementFile() throws Projet_Exception {
		Plateau pl = new Plateau();
		Tuile t = new Tuile(2,5);
		Sprinteur s = new Sprinteur(3,1,3,5,t);
		s.changementFile(pl);
		
		assertEquals("Le sprinteur drvrait avoir change de file", 0, s.getFileC());
	}
	/**
	 * test de la methode copierSprinteur de Sprinteur
	 * @throws Projet_Exception
	 */
	@Test
	public void testCopierSprinteur() throws Projet_Exception {
		Tuile t = new Tuile(2,5);
		Sprinteur r = new Sprinteur(3,1,3,5,t);
		Sprinteur res = r.copierSprinteur();
		
		assertEquals("Le numero du joueur devrait etre 3", 3, res.getNumJoueur());
		assertEquals("Le numero de sa file devrait etre 1", 1, res.getFileC());
		assertEquals("La positon devrait etre 3", 3, res.getPos());
		assertEquals("La vitesse devrait etre 5", 5, res.getVitesse());
		assertEquals("Le sprinteur devrait etre pr�sent sur la tuile", t, res.getTuilePresent());
	}
}
