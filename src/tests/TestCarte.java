package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import projet.Carte;
/**
 * Creation de la classe de tests de la classe Carte
 *
 */
public class TestCarte {
	/**
	 * test du constructeur avec une vitesse egale a 2
	 */
	@Test
	public void testConstructeurVitesse1() {
		Carte c = new Carte(2);
		
		assertEquals("La pioche devrait etre egale a 0", 0, c.getNombreDansPioche());
		assertEquals("La defausse devrait etre egale a 0", 0, c.getNombreDansDefausse());
		assertEquals("La vitesse devrait etre egale a 2", 2, c.getVitesse());
	}
	/**
	 * test du constructeur avec un vitesse quelconque
	 */
	@Test
	public void testConstructeurVitesse2() {
		Carte c = new Carte(5);
		
		assertEquals("La pioche devrait etre egale a 3", 3, c.getNombreDansPioche());
		assertEquals("La defausse devrait etre egale a 0", 0, c.getNombreDansDefausse());
		assertEquals("La vitesse devrait etre egale a 5", 5, c.getVitesse());
	}
	/**
	 * test de la methode toString
	 */
	@Test
	public void testToString() {
		Carte c = new Carte(4);
		
		assertEquals("La string devrait renvoyer Carte-V4", "Carte-V4 ", c.toString());
	}
}
