package projet;

import java.io.Serializable;


public abstract class Cycliste implements Serializable {
	/**
	 * attributs entier pour savoir les informations du cycliste
	 * numJoueur, numero du joueur 
	 * fileC, numero de la file ou se situe le cycliste
	 * pos, position du cycliste sur la tuile, savoir sa case
	 * vitesse, savoir la vitesse du cycliste 
	 */
	private int numJoueur,fileC,pos,vitesse; //File : 0 file de gauche, 1 file de droite ; pos = index
	/**
	 * attribut Tuile pour savoir sur quelle tuile est le cycliste 
	 */
	private Tuile tuilePresent;
	/**
	 * attribut boolean indiquant si le Cycliste a atteint l'arrivee
	 */
	private boolean fin;
	
	/**
	 * Constructeur Cycliste qui construit un cycliste
	 * @param n, numero du joueur
	 * @param f, numero de la file du cycliste
	 * @param pos, numero de la case du cycliste
	 * @param v, vitesse du cycliste
	 * @param tp, tuile sur laquelle est present le cycliste
	 */
	public Cycliste(int n, int f, int pos,int v,Tuile tp) {
		this.numJoueur=n;
		this.fileC=f;
		this.pos=pos;
		this.tuilePresent=tp;
		this.fin = false;
		this.vitesse = v;
	}
	/**
	 * methode qui regarde si le cycliste est a la fin de la piste
	 * @return vrai s'il est a la fin de la piste ou faux s'il ne l'est pas
	 */
	public boolean isFin() {
		return fin;
	}
	/**
	 * methode setter qui initialise le cycliste a la fin de la piste
	 * @param fin
	 */
	public void setFin(boolean fin) {
		this.fin = fin;
	}
	/**
	 * methode getter pour recuperer la position du cycliste
	 * @return la position du cycliste
	 */
	public int getPos() {
		return pos;
	}
	/**
	 * methode setter pour initialiser la position du cycliste
	 */
	public void setPos(int pos) {
		this.pos = pos;
	}
	/**
	 * methode getter pour recuperer la vitesse du cycliste
	 * @return la vitesse du cycliste
	 */
	public int getVitesse() {
		return vitesse;
	}
	/**
	 * methode setter pour initialiser la vitesse du cycliste
	 */
	public void setVitesse(int vitesse) {
		this.vitesse = vitesse;
	}
	/**
	 * methode getter pour recuperer la tuile sur laquelle est present 
	 * le cycliste
	 * @return la Tuile ou est present le cycliste
	 */
	public Tuile getTuilePresent() {
		return tuilePresent;
	}
	/**
	 * methode setter pour initialiser la tuile sur laquelle sera present 
	 * le cycliste
	 */
	public void setTuilePresent(Tuile tuilePresent) {
		this.tuilePresent = tuilePresent;
	}
	/**
	 * methode getter pour recuperer le num�ro du joueur
	 * @return le numero du joueur
	 */
	public int getNumJoueur() {
		return numJoueur;
	}
	/**
	 * methode setter pour initialiser le numero du joueur
	 */
	public void setNumJoueur(int numJoueur) {
		this.numJoueur = numJoueur;
	}
	/**
	 * methode getter pour recuperer la file ou est positionne le cycliste
	 * @return la position du cycliste
	 */
	public int getFileC() {
		return fileC;
	}
	/**
	 * methode setter pour initialiser la file ou sera positionne le cycliste
	 */
	public void setFileC(int fileC) {
		this.fileC = fileC;
	}
	
	public String toString() {
		String res ="";
		boolean cote=true;
		if(this.getTuilePresent().getTypeTuile()==2) {
			for(int k = 0; k<6;k++) {
				for(int i = 0; i<4;i++) {
					for(int j = 0; j<5;j++) {
						if(!cote && j==4) {
							cote=true;
						}
						if(i==0 || i==3) {
							res=res+"=";
						}else if(cote) {
							res=res+"|";
							cote=false;
						}else if(!cote) {
							res=res+" ";
						}
					}
					if(!(i==4)) {
						res=res+"\n";
					}
					cote=true;
				}
			}
		}
		return res;
	}
	/**
	 * methode abstraite qui deplace le cycliste en fonction du nombre sur la carte tiree
	 * @param pl, plateau de jeu sur lequel evolue les cyclistes
	 * @throws Projet_Exception
	 */
	public abstract void deplacement(Plateau pl) throws Projet_Exception;
	/**
	 * methode abstraite qui fait avancer le cycliste 
	 * @param pl, plateau de jeu
	 */
	public abstract void avancer(Plateau pl);
	/**
	 * methode qui fait changer de file le cycliste
	 * @param pl, plateau de jeu
	 */
	public abstract void changementFile(Plateau pl);
	/**
	 * methode qui regarde si le cycliste peut avancer
	 * @param pl, plateau de jeu
	 * @return vrai si le cycliste peut avancer
	 */
	public boolean avancerPossible(Plateau pl) {
		boolean res=true;
		int file = this.getFileC();

		if(((this.getPos()+1 > 5 && this.getTuilePresent().getTypeTuile() == 2) 
				|| (this.getPos()+1 > 1 && (this.getTuilePresent().getTypeTuile()==0 || this.getTuilePresent().getTypeTuile()==1)))){

			if(file == 0) {
				if(pl.getTuile()[this.getTuilePresent().getPositionSurPlateau()+1].getFile()[file][0] != null && 
						pl.getTuile()[this.getTuilePresent().getPositionSurPlateau()+1].getFile()[file+1][0] != null ) {
					res=false;
				}
			}else {
				if(pl.getTuile()[this.getTuilePresent().getPositionSurPlateau()+1].getFile()[file][0] != null && 
						pl.getTuile()[this.getTuilePresent().getPositionSurPlateau()+1].getFile()[file-1][0] != null ) {
					res=false;
				}
			}

		}else {
			if(file == 0) {
				if(this.getTuilePresent().getFile()[file][this.getPos()+1] != null && this.getTuilePresent().getFile()[file+1][this.getPos()+1] != null ) {
					res=false;
				}
			}else {
				if(this.getTuilePresent().getFile()[file][this.getPos()+1] != null && this.getTuilePresent().getFile()[file-1][this.getPos()+1] != null ) {
					res=false;
				}
			}
		}

		return res;

	}
	/**
	 * methode qui regarde si il y a un cycliste devant lui
	 * @return vrai s'il y a un cycliste devant lui
	 */
	public boolean detecterCyclisteDevant() {
		boolean res = false;
		if(this.getTuilePresent().getFile()[this.getFileC()][this.getPos()+1]!=null) {
			res=true;
		}
		return res;
	}
}
