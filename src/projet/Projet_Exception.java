package projet;
/**
 * Creation de la classe Projet_Exception qui cree notre propre exception
 *
 */
public class Projet_Exception extends Exception {
	/**
	 * Constructeur d'une exception Projet_Exception
	 */
	public Projet_Exception() {
		super();
	}
	/**
	 * Constructeur d'une exception Projet_Exception avec une message d'erreur
	 * @param message, message d'erreur a afficher
	 */
	public Projet_Exception(String message) {
		super(message);
	}
}
