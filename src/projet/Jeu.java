package projet;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Collections;
import java.util.Comparator;
import java.util.List; 

/**
 * Cr�ation de la classe Jeu qui cr�e le jeu et ses actions
 */
public class Jeu implements Serializable{
	/**
	 * Attribut entier contenant le nombre de joueur
	 */
	private int nombreDeJoueur;
	/**
	 * attribut Joueur contenant les infos des joueurs dans un tableau
	 */
	private Joueur[] joueurs;
	/**
	 * attribut ArrayList de Joueur qui donne l'ordre dans lequel les joueurs vont jouer
	 */
	private ArrayList<Joueur> deroulement; // ordre dans lequel les joueurs vont jouer
	/**
	 * attribut Plateau representant le plateau de jeu
	 */
	private Plateau plateau;
	/**
	 * attribut Joueur representant le joueur qui joue
	 */
	private Joueur joueurCourant;
	/**
	 * attribut Arraylist de Joueur qui represente le classement des joueur pendant la partie 
	 */
	private ArrayList<Joueur> classementJoueur;
	/**
	 * attribut Joueur qui represente le joueur qui a gagne 
	 */
	private Joueur gagnant;
	/**
	 * Constructeur qui cree le jeu 
	 * @param nbrJ, nombre de joueur
	 * @param j, tableau des infos des joueurs 
	 * @param pl, plateau de jeu
	 * @throws Projet_Exception
	 */
	public Jeu(int nbrJ, Joueur[] j, Plateau pl) throws Projet_Exception {
		if(nbrJ <= 1) {
			throw(new Projet_Exception("Nombre de joueurs insufisant"));
		}
		this.nombreDeJoueur = nbrJ;
		this.joueurs= new Joueur[j.length];
		for(int i = 0; i < j.length;i++) {
			this.joueurs[i] = j[i];
		}
		this.plateau=pl;
		this.deroulement = new ArrayList<Joueur>();
		this.classementJoueur = new ArrayList<Joueur>();
		this.gagnant = new Joueur();
	}
	
	public Jeu() {
		this.nombreDeJoueur=0;
		this.joueurs=null;
		this.plateau=null;
		this.deroulement= new ArrayList<Joueur>();
		this.classementJoueur = new ArrayList<Joueur>();
		this.gagnant = new Joueur();
	}
	/**
	 * methode getter pour recuperer le gagnant du jeu
	 * @return le joueur qui a gagne
	 */
	public Joueur getGagnant() {
		return gagnant;
	}
	/**
	 * methode setter pour initialiser le gagnant 
	 * @param gagnant, joueur qui a gagne 
	 */
	public void setGagnant(Joueur gagnant) {
		this.gagnant = gagnant;
	}
	/**
	 * methode getter qui recupere le joueur qui joue
	 * @return le joueur qui joue
	 */
	public Joueur getJoueurCourant() {
		return joueurCourant;
	}
	/**
	 * methode setter qui initilise le joueur qui joue
	 * @param joueurCourant, joueur qui est en train de jouer 
	 */
	public void setJoueurCourant(Joueur joueurCourant) {
		this.joueurCourant = joueurCourant;
	}


	/**
	 * methode qui initialise le jeu en configurant les joueurs
	 */
	public void caracteristiqueJoueur() { //FONCTIONNEL !!!
		//Joueur[] res = new Joueur[this.nombreDeJoueur];
		Scanner sc = new Scanner(System.in);
		int erreur=1;
		int test=0;
		int trouve = 0;
		boolean similaire = true;
		int random=0;
		String nom="";

		for(int i = 0; i < this.nombreDeJoueur ; i++) {
			// SELECTION DU NOM DE JOUEUR
			similaire=true;
			do {
				erreur=1;
				System.out.println("Entrez le nom du joueur "+(i+1)+" : ");
				nom = sc.next();
				this.joueurs[i].setNom(nom);
				try {
					test=Integer.parseInt(nom);
				}catch(NumberFormatException e) {
					erreur=0;
				}
				if(erreur == 1) {
					System.out.println("Caractere alphanumerique non autoris� !");
				}	
			}while(erreur==1);
			// ORDRE DES JOUEURS
			while(similaire && trouve != this.nombreDeJoueur) {
				random= (int)(Math.random()*((this.nombreDeJoueur-1+1))+1);
				similaire=false;
				for(int k=0; k < this.nombreDeJoueur;k++) {
					if(random==this.joueurs[k].getNumero()) {
						similaire=true;
					}
				}
				if(!similaire) {
					this.joueurs[i].setNumero(random);
					this.deroulement.add(this.joueurs[i]);
				}
			}
			trouve++;
		}
		Collections.sort(this.deroulement, new Comparator<Joueur>() {
			@Override
			public int compare(Joueur j1, Joueur j2) {
				return new Integer (j1.getNumero()).compareTo(j2.getNumero());
			}
		});
	}


	/**
	 * methode getter qui recupere la liste des actions que l'on peut faire 
	 * @return la liste des actions possibles
	 */
	public ArrayList<Joueur> getDeroulement() {
		return deroulement;
	}

	/**
	 * 
	 * METHODE QUI CONSISTE A PLACER LES CYCLISTES SUR LA TUILE 0 EN COMMENCANT 
	 * PAR LA DROITE SI PLUS DE PLACE ALORS FILE GAUCHE
	 */
	public void placementCycliste() {
		boolean placeDansFileDroite = false;
		boolean placer = false;
		int pion = 0;
		int compteurJoueur=0;
		int random2 = 0;

		while(compteurJoueur!=this.nombreDeJoueur) {


			for(int i = 0; i < 6; i++) {
				if(this.plateau.getTuile()[0].getFile()[0][i] == null) {
					placeDansFileDroite = true;
				}
			}

			if(placeDansFileDroite) {
				while(!placer) {
					random2= (int)(Math.random()*(5)+1);
					if(this.plateau.getTuile()[0].getFile()[0][random2] == null) {
						placer=true;
						if(pion == 0) {
							this.plateau.getTuile()[0].getFile()[0][random2] = new Rouleur(this.joueurs[compteurJoueur].getNumero(),0,random2,0,plateau.getTuile()[0]);
							this.joueurs[compteurJoueur].setRouleur((Rouleur)plateau.getTuile()[0].getFile()[0][random2]);
							pion++;
						}else {
							this.plateau.getTuile()[0].getFile()[0][random2] = new Sprinteur(this.joueurs[compteurJoueur].getNumero(),0,random2,0,plateau.getTuile()[0]);
							this.joueurs[compteurJoueur].setSprinteur((Sprinteur)plateau.getTuile()[0].getFile()[0][random2]);
							pion++;
						}

					}
				}
			}else {
				while(!placer) {
					random2= (int)(Math.random()*(5)+1);
					if(this.plateau.getTuile()[0].getFile()[1][random2] == null) {
						placer=true;
						if(pion == 0) {
							this.plateau.getTuile()[0].getFile()[1][random2] = new Rouleur(this.joueurs[compteurJoueur].getNumero(),0,random2,0,plateau.getTuile()[0]);
							this.joueurs[compteurJoueur].setRouleur((Rouleur)plateau.getTuile()[0].getFile()[1][random2]);
							pion++;
						}else {
							this.plateau.getTuile()[0].getFile()[1][random2] = new Sprinteur(this.joueurs[compteurJoueur].getNumero(),0,random2,0,plateau.getTuile()[0]);
							this.joueurs[compteurJoueur].setSprinteur((Sprinteur)plateau.getTuile()[0].getFile()[1][random2]);
							pion++;
						}

					}
				}
			}
			if(pion==2) {
				compteurJoueur++;
				pion=0;
			}
			placer=false;
			placeDansFileDroite=false;
		}
	}
	/**
	 * methode permettant de tirer une carte dans la pioche Rouleur
	 * @param j, Joueur devant tirer une carte
	 * @return vitesse de la carte tiree
	 */
	public int tirerPiocheRouleur(Joueur j) {
		Scanner sc = new Scanner(System.in);
		int choix = 0;
		ArrayList<Carte> tirage = new ArrayList<Carte>();
		tirage= j.getPioche().getPiRouleur().tirerCarte();
		System.out.println("Voici les cartes tir�s :");
		for(int i = 0; i < tirage.size();i++) {
			System.out.print("\n"+i+"."+tirage.get(i).toString()+"\n");
		}
		//FAIRE EXCEPTION
		int erreur=0;
		do {
			erreur=0;
			System.out.println("\nQuel carte souhaitez-vous selectionner ? (Entrez son numero)");
			choix=sc.nextInt();
			try {
				if(choix > 3) {
					erreur=1;
					throw(new Projet_Exception("\nEntrez un chiffre entre 0 et 3 !"));
				}
			}catch(Projet_Exception e) {
				System.out.println(e.toString());
			}
		}while(erreur==1);
		return (tirage.get(choix).getVitesse());

	}
	/**
	 * methode permettant de tirer une carte dans la pioche Sprinteur
	 * @param j, Joueur devant tirer une carte
	 * @return vitesse de la carte tiree
	 */
	public int tirerPiocheSprinteur(Joueur j) {
		Scanner sc = new Scanner(System.in);
		int choix = 0;
		ArrayList<Carte> tirage = new ArrayList<Carte>();
		tirage= j.getPioche().getPiSprinteur().tirerCarte();
		System.out.println("Voici les cartes tir�s :");
		for(int i = 0; i < tirage.size();i++) {
			System.out.print("\n"+i+"."+tirage.get(i).toString()+"\n");
		}
		int erreur=0;
		do {
			erreur=0;
			System.out.println("\nQuel carte souhaitez-vous selectionner ? (Entrez son numero)");
			choix=sc.nextInt();
			try {
				if(choix > 3) {
					erreur=1;
					throw(new Projet_Exception("\nEntrez un chiffre entre 0 et 3 !"));
				}
			}catch(Projet_Exception e) {
				System.out.println(e.toString());
			}
		}while(erreur==1);

		
		return (tirage.get(choix).getVitesse());

	}
	/**
	 * methode getter qui recupere le classement des joueurs 
	 * @return la liste des joueurs class�e 
	 */
	public ArrayList<Joueur> getClassementJoueur() {
		return classementJoueur;
	}
	/**
	 * methode setter permettant d'initialiser le classement des joueurs 
	 * @param classementJoueur, classement des joueurs 
	 */
	public void setClassementJoueur(ArrayList<Joueur> classementJoueur) {
		this.classementJoueur = classementJoueur;
	}
	/**
	 * methode qui applique le principe d'aspiration aux Cyclistes
	 * @throws Projet_Exception
	 */
	public void aspiration() throws Projet_Exception {//Applique l'aspiration a tout le plateau de jeu
		ArrayList<Cycliste> groupe = new ArrayList<Cycliste>();
		Cycliste teteGroupe=new Rouleur(this.plateau.getTuile()[0]);

		for(int i = 0; i < this.getPlateau().getTuile().length;i++) { // On parcourt toutes les tuiles du plateau
			for(int j = 0; j < this.getPlateau().getTuile()[i].getFile()[0].length;j++) {//On parcourt les cases case par case en alternant les files

				if(this.getPlateau().getTuile()[i].getFile()[0][j] != null) {
					groupe=formerGroupe(this.getPlateau().getTuile()[i].getFile()[0][j]);
					
					Cycliste tete=trouverTeteGroupe(groupe);
					Collections.sort(groupe, comparer);
					
					if((tete.getPos()+2 > 5 && this.getPlateau().getTuile()[i].getTypeTuile() == 2) || 
							tete.getPos()+2 > 1 && (this.getPlateau().getTuile()[i].getTypeTuile() == 1 || this.getPlateau().getTuile()[i].getTypeTuile() == 0)) {
						
						if(this.getPlateau().getTuile()[i+1].getFile()[tete.getFileC()][1] != null) {
							for(int k = 0; k < groupe.size(); k++) {
								groupe.get(k).avancer(this.getPlateau());
							}
						}
						
					}else {
						if(this.getPlateau().getTuile()[i].getFile()[tete.getFileC()][tete.getPos()+2] != null) {
							for(int k = 0; k < groupe.size(); k++) {
								groupe.get(k).avancer(this.getPlateau());
							}
						}
					}
				}

				


			}
		}







	}
	

	/**
	 * methode qui regarde quel cycliste est en tete du groupe
	 * @param c, liste des cyclistes
	 * @return le cycliste en tete du groupe 
	 */
	public Cycliste trouverTeteGroupe(ArrayList<Cycliste> c) {
		ArrayList<Cycliste> lc = new ArrayList<Cycliste>();
		int max=-1;
		for(int i = 0; i < c.size(); i++) {
			if(c.get(i).getPos() > max) {
				max=c.get(i).getPos();
				lc.add(0,c.get(i));
			}
		}
		Cycliste cyc = lc.get(0);
		return cyc;
	}
	
	/**
	 * methode permmettant de former un groupe a partir d'un cycliste 
	 * @param c, cycliste
	 * @return le groupe de cycliste
	 */
	public ArrayList<Cycliste> formerGroupe(Cycliste c){
		ArrayList<Cycliste> res = new ArrayList<Cycliste>();
		boolean trouve = true;
		int iterateur = 1;
		int iterateur2 = 1;
		
		if(c instanceof Rouleur) {

			Rouleur r = (Rouleur) c;

			res.add(r);

			while(r.getTuilePresent().getFile()[r.getFileC()][r.getPos()+iterateur] != null) { // Tant qu'il y � des cyclistes devant lui
				Cycliste membre = r.getTuilePresent().getFile()[r.getFileC()][r.getPos()+iterateur];
				res.add(membre);
				if(membre.getFileC()==0) {
					if(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur] != null) {
						res.add(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur]);
					}
				}else if(membre.getFileC()==1) {
					if(r.getTuilePresent().getFile()[r.getFileC()-1][r.getPos()+iterateur] != null) {
						res.add(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur]);
					}
				}


				if(r.getFileC() == 0) {//On verifie dans quel file on se trouve
					if(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur] != null) {//Si il y a un cycliste dans la file oppos� au cycliste 

						if(!res.contains(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur])) {//Si le cycliste n'est pas dans la liste, on l'ajoute

							res.add(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur]);

						}

						while(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur2] != null) {//Tant qu'il y a des cycliste devant le cycliste de la file oppos� 
						

							if(!res.contains(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur2])) {//Si le cycliste n'est pas dans la liste, on l'ajoute

								res.add(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur2]);
							}
							iterateur2++;
						}
					}
				}else if(r.getFileC() == 1) {// Meme chose pour l'autre file

					if(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur] != null) {//Si il y a un cycliste dans la file oppos� au cycliste 

						if(!res.contains(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur])) {//Si le cycliste n'est pas dans la liste, on l'ajoute

							res.add(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur]);

						}

						while(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur2] != null) {//Tant qu'il y a des cycliste devant le cycliste de la file oppos� 

							if(!res.contains(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur2])) {//Si le cycliste n'est pas dans la liste, on l'ajoute

								res.add(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur2]);
							}
							iterateur2++;
						}

					}
				}

					iterateur++;
				}

			}else {//cas sprinteur

				Sprinteur r = (Sprinteur) c;

				res.add(c);

				while(r.getTuilePresent().getFile()[r.getFileC()][r.getPos()+iterateur] != null) { // Tant qu'il y � des cyclistes devant lui
					Cycliste membre = r.getTuilePresent().getFile()[r.getFileC()][r.getPos()+iterateur];
					res.add(membre);
					if(membre.getFileC()==0) {
						if(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur] != null) {
							res.add(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur]);
						}
					}else if(membre.getFileC()==1) {
						if(r.getTuilePresent().getFile()[r.getFileC()-1][r.getPos()+iterateur] != null) {
							res.add(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur]);
						}
					}


					if(r.getFileC() == 0) {//On verifie dans quel file on se trouve
						if(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur] != null) {//Si il y a un cycliste dans la file oppos� au cycliste 

							if(!res.contains(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur])) {//Si le cycliste n'est pas dans la liste, on l'ajoute

								res.add(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur]);

							}

							while(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur2] != null) {//Tant qu'il y a des cycliste devant le cycliste de la file oppos� 

								if(!res.contains(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur2])) {//Si le cycliste n'est pas dans la liste, on l'ajoute

									res.add(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur2]);
								}
							}
							iterateur2++;
						}
					}else if(r.getFileC() == 1) {// Meme chose pour l'autre file

						if(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur] != null) {//Si il y a un cycliste dans la file oppos� au cycliste 

							if(!res.contains(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur])) {//Si le cycliste n'est pas dans la liste, on l'ajoute

								res.add(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur]);

							}

							while(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur2] != null) {//Tant qu'il y a des cycliste devant le cycliste de la file oppos� 

								if(!res.contains(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur2])) {//Si le cycliste n'est pas dans la liste, on l'ajoute

									res.add(r.getTuilePresent().getFile()[r.getFileC()+1][r.getPos()+iterateur2]);
								}
							}
							iterateur2++;

						}
					}

						iterateur++;
					}
			}
		return res;
	}

		/**
		 * methode getter qui recupere le plateau de jeu
		 * @return le plateau de jeu
		 */
		public Plateau getPlateau() {
			return plateau;
		}
		/**
		 * methode setter qui initialise le plateau de jeu
		 * @param plateau, plateau de jeu
		 */
		public void setPlateau(Plateau plateau) {
			this.plateau = plateau;
		}

		/**
		 * methode qui met a jour le classement des joueurs a partir des cyclistes
		 * @return le classement des joueurs
		 */
		public ArrayList<Joueur> miseAjourClassement() {
			int numJ = 0;
			ArrayList<Joueur> res = new ArrayList<Joueur>();
			ArrayList<Cycliste> cycTeste = new ArrayList<Cycliste>();
			
			for(int i = 0; i < this.plateau.getTuile().length;i++) {
				for(int j = 0; j < this.plateau.getTuile()[i].getFile().length;j++) {
					for(int k = 0; k < this.plateau.getTuile()[i].getFile()[j].length;k++) {
						
						if(this.plateau.getTuile()[i].getFile()[j][k] != null){
							Cycliste cyc = this.plateau.getTuile()[i].getFile()[j][k];
							//On regarde a quel joueur le cycliste appartient
							Joueur joueur = new Joueur();
							boolean trouve = false;
							int iterateur=0;
							System.out.println("NUM JOUEUR CYC :"+cyc.getNumJoueur());
							System.out.println("NUM JOUEUR JOUEUR :"+this.getDeroulement().get(1).getNumero());
							while(!trouve && iterateur < this.getDeroulement().size()) {
								if(this.getDeroulement().get(iterateur).getNumero() == cyc.getNumJoueur()) {
									joueur = this.joueurs[iterateur];
									trouve=true;
								}
								iterateur++;
							}
							trouve=false;
							iterateur=0;
							
							if(res.contains(joueur)) {//Si on � d�ja croiser un cycliste du joueur (si oui, le joueur est d�ja present dans la liste)
								Cycliste cycPresent=new Sprinteur();
								//Recherche du cycliste deja teste du meme joueur
								while(!trouve && iterateur < cycTeste.size()) {
									if(cycTeste.get(iterateur).getNumJoueur() == cyc.getNumJoueur()) {
										cycPresent = cycTeste.get(iterateur);
										trouve=true;
									}
									iterateur++;
								}
								trouve=false;
								iterateur=0;
								if((cyc.getTuilePresent().getPositionSurPlateau() == cycPresent.getTuilePresent().getPositionSurPlateau())
										&& cyc.getPos() > cycPresent.getPos() ) {//On regarde quel cycliste est le plus avance
									res.remove(joueur); // On supprime le joueur pour modifier sa place dans la liste
									res.add(joueur); //On place le joueur en fin de liste
									
									System.out.println("Joueur ajoute :"+joueur.getNom());
									
									cycTeste.add(cyc);
								}else if(cyc.getTuilePresent().getPositionSurPlateau() > cycPresent.getTuilePresent().getPositionSurPlateau()) {//On regarde si le cycliste trouve est sur une tuile plus lointaine
									res.remove(joueur); // On supprime le joueur pour modifier sa place dans la liste
									res.add(joueur); //On place le joueur en fin de liste
									
									System.out.println("Joueur ajoute :"+joueur.getNom());
									cycTeste.add(cyc);
								}else {
									cycTeste.add(cyc);
								}
							}else {
								res.remove(joueur); // On supprime le joueur pour modifier sa place dans la liste
								res.add(joueur); //On place le joueur en fin de liste
								
								System.out.println("Joueur ajoute :"+joueur.getNom());
								cycTeste.add(cyc);
							}
								
							}
						}
				}
			}
			this.setClassementJoueur(res);
			return res;
		}
		/**
		 * methode qui permet de comparer des cyclistes
		 */
		public static Comparator<Cycliste> comparer = new Comparator<Cycliste>() {
			@Override
			public int compare(Cycliste c1, Cycliste c2) {
				return (int) (c2.getPos() - c1.getPos());
			}
		};
		/**
		 * methode qui permet de choisir le pion que l'on veut jouer
		 * @return le numero du pion que l'on veut jouer
		 */
		public int choisirPion() {
			int choix=0;
			int erreur=0;
			String test = "";
			Scanner sc = new Scanner(System.in);
			
			do {
				erreur=0;
				System.out.println("Quel pion voulez vous jouer en premier ?\n\n 1. Rouleur\n 2.Sprinteur");
				choix = sc.nextInt();
				test=choix+"";
				try {
					choix=Integer.parseInt(test);
				}catch(NumberFormatException e) {
					erreur=1;
				}
				if(erreur == 1) {
					System.out.println("Entrez un chiffre !");
				}	
			}while(erreur==1);
			
			return choix;
			
		}
		/**
		 * methode permettant d'attribuer une carte fatigue 
		 * @param pl, plateau de jeu 
		 */
		public void attribuerCarteFatigue(Plateau pl) {
			boolean trouve = false;
			int iterateur = 0;
			for(int i = 0; i < this.deroulement.size();i++) {
				Joueur j = this.deroulement.get(i);
				if(!(this.deroulement.get(i).isArrivee())){
					//CAS DU ROULEUR
					if((j.getRouleur().getPos()+1 > 5 && j.getRouleur().getTuilePresent().getTypeTuile() == 2) || 
							(j.getRouleur().getPos()+1 > 1 && (j.getRouleur().getTuilePresent().getTypeTuile() == 0 || j.getRouleur().getTuilePresent().getTypeTuile() == 1))  ) {
						
						if(pl.getTuile()[j.getRouleur().getTuilePresent().getPositionSurPlateau()+1].getFile()[j.getRouleur().getFileC()][0] == null) {
							
							while(!trouve && iterateur < j.getPioche().getPiRouleur().getPiocheRouleur().size()) {//Recherche des cartes a 2 de vitesse
								
								if(j.getPioche().getPiRouleur().getPiocheRouleur().get(iterateur).getVitesse() == 2) {
									j.getPioche().getPiRouleur().getPiocheRouleur().get(iterateur).setNombreDansDefausse(
											j.getPioche().getPiRouleur().getPiocheRouleur().get(iterateur).getNombreDansDefausse()+1);
									trouve=true;
								}
								iterateur++;
							}
							trouve=false;

						}
						
					}else {
						if(j.getRouleur().getTuilePresent().getFile()[j.getRouleur().getFileC()][j.getRouleur().getPos()+1] == null) {
							while(!trouve && iterateur < j.getPioche().getPiRouleur().getPiocheRouleur().size()) {
								
								if(j.getPioche().getPiRouleur().getPiocheRouleur().get(iterateur).getVitesse() == 2) {
									
									j.getPioche().getPiRouleur().getPiocheRouleur().get(iterateur).setNombreDansDefausse(
											j.getPioche().getPiRouleur().getPiocheRouleur().get(iterateur).getNombreDansDefausse()+1);
									trouve=true;
								}
								iterateur++;
							}
							trouve=false;

						}
					}
					//CAS DU SPRINTEUR
					if((j.getSprinteur().getPos()+1 > 5 && j.getSprinteur().getTuilePresent().getTypeTuile() == 2) || 
							(j.getSprinteur().getPos()+1 > 1 && (j.getSprinteur().getTuilePresent().getTypeTuile() == 0 || j.getSprinteur().getTuilePresent().getTypeTuile() == 1))  ) {
						
						if(pl.getTuile()[j.getSprinteur().getTuilePresent().getPositionSurPlateau()+1].getFile()[j.getSprinteur().getFileC()][0] == null) {
							while(!trouve && iterateur < j.getPioche().getPiSprinteur().getPiocheSprinteur().size()) {
								
								if(j.getPioche().getPiSprinteur().getPiocheSprinteur().get(iterateur).getVitesse() == 2) {
									
									j.getPioche().getPiSprinteur().getPiocheSprinteur().get(iterateur).setNombreDansDefausse(
											j.getPioche().getPiSprinteur().getPiocheSprinteur().get(iterateur).getNombreDansDefausse()+1);
									trouve=true;
								}
								iterateur++;
							}
							trouve=false;

						}
						
					}else {
						if(j.getSprinteur().getTuilePresent().getFile()[j.getSprinteur().getFileC()][j.getSprinteur().getPos()+1] == null) {
							while(!trouve && iterateur < j.getPioche().getPiSprinteur().getPiocheSprinteur().size()) {
								if(j.getPioche().getPiSprinteur().getPiocheSprinteur().get(iterateur).getVitesse() == 2) {
									j.getPioche().getPiSprinteur().getPiocheSprinteur().get(iterateur).setNombreDansDefausse(
											j.getPioche().getPiSprinteur().getPiocheSprinteur().get(iterateur).getNombreDansDefausse()+1);
									trouve=true;
								}
								iterateur++;
							}
							trouve=false;

						}
					}
					
					

				}
			}
		}
		/**
		 * methode qui permet de savoir si un cycliste est arrive 
		 * @return vrai si le cycliste est arrive ou faux si il est pas arrive 
		 */
		public boolean arriver() {
			boolean res = false;
			boolean trouve = false;
			int iterateur = 0;
			for(int i = 0; i < this.plateau.getTuile()[this.plateau.getTuile().length-1].getFile().length;i++) {
				for(int j = 0; j < this.plateau.getTuile()[this.plateau.getTuile().length-1].getFile()[i].length;j++ ) {
					Cycliste actuel = this.plateau.getTuile()[this.plateau.getTuile().length-1].getFile()[i][j];
					if(actuel != null) {
						res = true;
						while(!trouve) {
							if(this.deroulement.get(iterateur).getNumero() == actuel.getNumJoueur()) {
								trouve=true;
								this.gagnant = this.deroulement.get(iterateur);
							}
						}
					}
				}

			}
			return res;
		}
		/**
		 * methode permettant d'effacer ce qu'il y a dans la console
		 * @return un string vide pour effacer la console
		 */
		public final static String effacerConsole() {
			String res = "";
			for(int i = 0; i<50;i++) {
				res+="\n";
			}
			return res;
		}
		/**
		 * methode qui permet de sauvegarder une partie en cours 
		 */
		public void sauvegarder() {
			try {
				ObjectOutputStream jeu = new ObjectOutputStream(new FileOutputStream("../sauvegardeJeu.txt"));
				jeu.writeInt(nombreDeJoueur);
				jeu.writeObject(joueurs);
				jeu.writeObject(deroulement);
				jeu.writeObject(plateau);
				jeu.writeObject(joueurCourant);
				jeu.writeObject(classementJoueur);
				jeu.writeObject(gagnant);
				jeu.close();
			}catch(IOException e) {
				System.out.println("erreur d'E/S");
			}catch(Exception e) {
				System.out.println("erreur hors E/S");
			}
		}
		/**
		 * methode qui permet de charger une partie sauvegardee
		 */
		public void charger() {
			try {
				ObjectInputStream jeu = new ObjectInputStream(new FileInputStream("../sauvegardeJeu.txt"));
				this.nombreDeJoueur=(int)(jeu.readInt());
				this.joueurs=(Joueur[])(jeu.readObject());
				this.deroulement=(ArrayList<Joueur>)(jeu.readObject());
				this.plateau=(Plateau)(jeu.readObject());
				this.joueurCourant=(Joueur)(jeu.readObject());
				this.classementJoueur=(ArrayList<Joueur>)(jeu.readObject());
				this.gagnant=(Joueur)(jeu.readObject());
				jeu.close();
			}catch(IOException e) {
				System.out.println("erreur d'E/S");
				e.printStackTrace();
			}catch(Exception e) {
				System.out.println("erreur hors E/S");
			}
		}
	}

	