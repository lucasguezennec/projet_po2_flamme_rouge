package projet;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * Creation de la classe PiocheSprinteur qui cree la pioche d'un joueur 
 * pour un sprinteur
 * 
 */
public class PiocheSprinteur implements Serializable {
	/**
	 * attribut ArrayList qui est une liste de carte qui constitue la pioche pour le sprinteur
	 */
	private ArrayList<Carte> piocheSprinteur;
	/**
	 * Constructeur qui cree une pioche pour le sprinteur
	 */
	public PiocheSprinteur() {
		this.piocheSprinteur = new ArrayList<Carte>();
		for(int i = 2; i<6;i++) {
		this.piocheSprinteur.add(new Carte(i));
		}
		this.piocheSprinteur.add(new Carte(9));
		this.piocheSprinteur.add(new Carte(2));
	}
	
	/**
	 * methode getter qui recupere la pioche pour un sprinteur
	 * @return la pioche du sprinteur
	 */
	public ArrayList<Carte> getPiocheSprinteur() {
		return piocheSprinteur;
	}
	/**
	 * methode permettant de tirer une carte dans la pioche du sprinteur
	 * @return les cartes tirees par le joueur
	 */
	public ArrayList<Carte> tirerCarte() {
		ArrayList<Carte> tirage = new ArrayList<Carte>();
		int compteurCarteRestante = 0;
		int sommeDesCartes=0;
		for(int i = 0; i<this.getPiocheSprinteur().size();i++) {
			sommeDesCartes+=this.getPiocheSprinteur().get(i).getNombreDansPioche();
		}
		for(int i = 0; i<this.getPiocheSprinteur().size();i++) {
			if(this.getPiocheSprinteur().get(i).getNombreDansPioche() > 0) {
				compteurCarteRestante++;
			}
		}
		if(compteurCarteRestante >= 4) {
			tirage=this.tirageAleatoireProba(sommeDesCartes);
		}else {
			for(int i = 0; i<this.getPiocheSprinteur().size();i++) {
				if(this.getPiocheSprinteur().get(i).getNombreDansPioche() != 0) {
					tirage.add(this.getPiocheSprinteur().get(i));
				}
				this.swapPiocheDefausse();
				tirage.addAll(this.tirageAleatoireProba(sommeDesCartes));
			}
		}
		return tirage;
	}
	/**
	 * methode permettant de determiner la carte tiree selon leurs nombres
	 * @param sommeDesCartes, sommes des cartes d'un m�me type
	 * @return les cartes tirees par le joueur 
	 */
	public ArrayList<Carte> tirageAleatoireProba(int sommeDesCartes){
		ArrayList<Carte> res = new ArrayList<Carte>();
		int trouve=0;
		int i = 0;
		int rdm = 0;
		while(trouve!=4) {
			rdm=(int)(Math.random() * this.getPiocheSprinteur().size());
			if(this.getPiocheSprinteur().get(rdm).getNombreDansPioche() != 0) {
				trouve++;
				res.add(this.getPiocheSprinteur().get(rdm));
				this.getPiocheSprinteur().get(rdm).setNombreDansPioche(this.getPiocheSprinteur().get(rdm).getNombreDansPioche()-1);
				this.getPiocheSprinteur().get(rdm).setNombreDansDefausse(this.getPiocheSprinteur().get(rdm).getNombreDansDefausse()+1);
			}
			
		}
		return res;
	}

	/**
	 * methode permettant d'echanger la pioche du sprinteur et sa defausse 
	 */
	public void swapPiocheDefausse() {
		for(int i = 0; i < this.getPiocheSprinteur().size();i++) {
			this.getPiocheSprinteur().get(i).setNombreDansPioche(this.getPiocheSprinteur().get(i).getNombreDansDefausse());
			this.getPiocheSprinteur().get(i).setNombreDansDefausse(0);
		}
	}
	/**
	 * methode permettant de retirer une carte au joueur
	 * @param j, Joueur a qui retirer la carte
	 */
	public void retirerCarteJouer(Joueur j) {
		int vitesse = j.getRouleur().getVitesse();
		boolean trouve = false;
		int i = 0;
		while(!trouve && i < this.getPiocheSprinteur().size()) {
			if(this.getPiocheSprinteur().get(i).getVitesse() == vitesse) {
				this.getPiocheSprinteur().get(i).setNombreDansDefausse(this.getPiocheSprinteur().get(i).getNombreDansDefausse()-1);
				trouve=true;
			}
			i++;
		}
	}
	
	
	
}
