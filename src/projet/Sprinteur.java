package projet;

import java.io.Serializable;
import java.util.Scanner;
/**
 * Creation de la classe Sprinteur qui herite de la classe Cycliste et 
 * qui cree un sprinteur
 *
 */
public class Sprinteur extends Cycliste implements Serializable {

	/**
	 * Constructeur qui cree un sprinteur a partir de parametres donnes
	 * @param numJoueur, numero du joueur
	 * @param f, numero de la file du sprinteur
	 * @param pos, numero de la case ou est situe le sprinteur
	 * @param v, vitesse du sprinteur 
	 * @param tp, tuile sur laquelle est situe le sprinteur
	 */
	public Sprinteur(int numJoueur, int f,int pos,int v,Tuile tp) {
		super(numJoueur,f,pos,v,tp);
	}
	/**
	 * Constructeur qui cree un sprinteur a partir d'un sprinteur passe en parametre
	 * @param r, sprinteur
	 */
	public Sprinteur(Sprinteur r) {
		super(r.getNumJoueur(),r.getFileC(),r.getPos(),r.getVitesse(),r.getTuilePresent());
	}
	/**
	 * Constructeur qui initialise un cycliste vide
	 */
	public Sprinteur() {
		super(-1,-1,-1,-1,null);
	}


	/**
	 * methode permettant de deplacer le sprinteur du nombre inscrit sur la carte tiree
	 * @param pl, plateau sur lequel evoluent les sprinteurs
	 */
	public void deplacement(Plateau pl) throws Projet_Exception  { //NON TESTE
		int pointDeplacement = super.getVitesse();
		Scanner sc = new Scanner(System.in);
		int choix =0;
		int erreur=0;
		String test="";
		while(pointDeplacement != 0) {
			if(!this.avancerPossible(pl)) {
				System.out.println("\nAucun deplacement possible !");
				pointDeplacement=0;
			}else {
				do {
					erreur=1;
					System.out.println("Tuile avant deplacement : \n"+this.getTuilePresent());
					System.out.println("_Deplacement :\n1. Changer de file (0 point utilise)\n2. Avancer (1 point utilise)");
					choix=sc.nextInt();
					try {
						choix=Integer.parseInt(test);
						if(choix != 1 && choix != 2) {
							throw(new Projet_Exception("Entrez un nombre entre 1-2"));
						}
						if(this.detecterCyclisteDevant() && choix==2) {
							throw(new Projet_Exception("\nVous ne pouvez pas avancer car un cycliste est devant vous !\n"));
						}
					}catch(NumberFormatException e) {
						erreur=0;
					}
					catch(Projet_Exception e) {
						System.out.println(e.toString());
						erreur=1;
					}
				}while(erreur==1);

				switch(choix) {
				case 1://CHANGEMENT DE FILE
					this.changementFile(pl);
					break;
				case 2: //VRIFIER CYCLISTE DEVANT
					pointDeplacement--;
					this.avancer(pl);
					break;
				}
			}

			System.out.println(Jeu.effacerConsole());
			System.out.println("Coordonne du Sprinteur :"+this.getFileC()+this.getPos());
		}
	}
	/**
	 * methode permettant de faire avancer un sprinteur sur le plateau
	 * @param pl, plateau de jeu
	 */
	public void avancer(Plateau pl) {

		if(super.getPos() == 5 && super.getTuilePresent().getTypeTuile() ==2) {
			super.setPos(0);//MISE A JOUR ATTRIBUT
			super.setTuilePresent(pl.getTuile()[super.getTuilePresent().getPositionSurPlateau()+1]);//Changement de tuile
			super.getTuilePresent().getFile()[super.getFileC()][0]=this.copierSprinteur();// Copie du cycliste sur la case 
			pl.getTuile()[super.getTuilePresent().getPositionSurPlateau()-1].getFile()[super.getFileC()][5] =null; //Suppression de l'ancien cycliste
		}
		else if(super.getPos() == 1 && (super.getTuilePresent().getTypeTuile() == 0 || super.getTuilePresent().getTypeTuile() == 1)) {//Si fin de virage
			super.setPos(0);//MISE A JOUR ATTRIBUT
			super.setTuilePresent(pl.getTuile()[super.getTuilePresent().getPositionSurPlateau()+1]);//Changement de tuile
			super.getTuilePresent().getFile()[super.getFileC()][0]=this.copierSprinteur();// Copie du cycliste sur la case 
			pl.getTuile()[super.getTuilePresent().getPositionSurPlateau()-1].getFile()[super.getFileC()][1] =null; //Suppression de l'ancien cycliste

		}
		else {//AVANCEMENT DE +1
			super.setPos(super.getPos()+1);
			super.getTuilePresent().getFile()[super.getFileC()][super.getPos()]= this.copierSprinteur();//COPIE DU ROULEUR
			super.getTuilePresent().getFile()[super.getFileC()][super.getPos()-1]= null;//SUPPRESSION DU ROULEUR

		}

	}
	/**
	 * methode qui detecte s'il y a un cycliste devant
	 */
	public boolean detecterCyclisteDevant() {
		return super.detecterCyclisteDevant();
	}

	/**
	 * methode qui regarde s'il on peut avancer
	 */
	public boolean avancerPossible(Plateau pl) {
		return super.avancerPossible(pl);
	}

	/**
	 * methode permettant de faire changer de file un sprinteur
	 * @param pl, plateau de jeu
	 */
	public void changementFile(Plateau pl) {

		if(super.getFileC() == 0) {//PASSAGE A DROITE
			super.setFileC(1);
			super.getTuilePresent().getFile()[1][super.getPos()]= this.copierSprinteur();
			super.getTuilePresent().getFile()[0][super.getPos()]= null;
		}else {//PASSAGE A GAUCHE
			super.setFileC(0);
			super.getTuilePresent().getFile()[0][super.getPos()]= this.copierSprinteur();
			super.getTuilePresent().getFile()[1][super.getPos()]= null;
		}

	}

	/**
	 * methode permettant de copier le sprinteur actuel
	 * @return le sprinteur qui est copie  
	 */
	public Sprinteur copierSprinteur() {
		Sprinteur res = new Sprinteur(this.getNumJoueur(), this.getFileC(), this.getPos(),this.getVitesse(),this.getTuilePresent());
		return res;
	}

	public String toString() {
		return (super.getNumJoueur()+"-S");
	}



}
