package projet;
import java.util.*;
/**
 * Creation de la classe applicative Principale
 *
 */
public class Principale {
	/**
	 * methode main utilisee pour lancer le jeu 
	 * @param args
	 * @throws Projet_Exception
	 */
	public static void main(String[] args) throws Projet_Exception {
		//LIMITER NOMBRE DE JOUEUR A 4
		boolean fin = false;
		Scanner sc = new Scanner(System.in);
		int erreur= 0;
		int nbrJ=0;
		boolean chargerPartie = false;
		int choix = 0;
		Jeu game = new Jeu();
		String test="";
		do {
			erreur=0;
			System.out.println("___FLAMME_ROUGE___\n\n1.Nouvelle partie\n2.Charger partie");
			choix = sc.nextInt();
			if(choix > 2 || choix < 1) {
				erreur=1;
				System.out.println("Entrez un nombre entre 1-2 !");
			}else {
				test=choix+"";
				try {
					choix=Integer.parseInt(test);
				}catch(NumberFormatException e) {
					erreur=1;
				}
				if(erreur == 1) {
					System.out.println("Entrez un chiffre !");
				}	
			}
			
		}while(erreur==1);
		
		if(choix==1) {
			do {
				erreur=0;
				System.out.println("____FLAMME_ROUGE____\n\nCombien y'a t-il de joueur ? : ");
				nbrJ = sc.nextInt();
				if(nbrJ > 4 || nbrJ < 2) {
					erreur=1;
					System.out.println("\n2 joueurs MIN et 4 joueurs MAX !\n");
				}
			}while(erreur == 1);



			Joueur[] j = new Joueur[nbrJ];
			for(int i = 0; i < nbrJ;i++) {
				j[i]= new Joueur();
			}
			Plateau plateau = new Plateau();
			game = new Jeu(nbrJ,j,plateau);
			game.caracteristiqueJoueur();
			game.placementCycliste();
			System.out.println(Jeu.effacerConsole());
			System.out.println("\n DEBUT DE LA PARTIE !\n\n");
		}else {
			game.charger();
		}
		
		while(!fin) {
			System.out.println("\n__Phase_Energie__\n\n");
			for(int i = 0; i < game.getDeroulement().size(); i++) {
				game.setJoueurCourant(game.getDeroulement().get(i));
				if(!(game.getJoueurCourant().isArrivee())){
					System.out.println(Jeu.effacerConsole());
					System.out.println("\nC'est a "+game.getDeroulement().get(i).getNom()+" de jouer !");
					int choixPion = game.choisirPion();
					System.out.println(Jeu.effacerConsole());
					if(choixPion == 1) {
						if(!(game.getJoueurCourant().getRouleur().isFin())) {
							System.out.println(Jeu.effacerConsole());
							System.out.println("\nTuile actuel du Rouleur (vous �tes le joueur "+game.getJoueurCourant().getNumero()+"):\n"+game.getJoueurCourant().getRouleur().getTuilePresent()+"\n\n");
							System.out.println("\n__Tirage pour Rouleur !");
							game.getJoueurCourant().getRouleur().setVitesse(game.tirerPiocheRouleur(game.getJoueurCourant()));
						}
						if(!(game.getJoueurCourant().getSprinteur().isFin())) {
							System.out.println(Jeu.effacerConsole());
							System.out.println("\nTuile actuel du Sprinteur (vous �tes le joueur "+game.getJoueurCourant().getNumero()+"):\n"+game.getJoueurCourant().getSprinteur().getTuilePresent()+"\n\n");
							System.out.println("\n__Tirage pour Sprinteur !");
							game.getJoueurCourant().getSprinteur().setVitesse(game.tirerPiocheSprinteur(game.getJoueurCourant()));
						}
					}else {
						if(!(game.getJoueurCourant().getSprinteur().isFin())) {
							System.out.println(Jeu.effacerConsole());
							System.out.println("\nTuile actuel du Sprinteur (vous �tes le joueur "+game.getJoueurCourant().getNumero()+"):\n"+game.getJoueurCourant().getSprinteur().getTuilePresent()+"\n\n");
							System.out.println("\n__Tirage pour Sprinteur !");
							int vitesse = game.tirerPiocheSprinteur(game.getJoueurCourant());
							game.getJoueurCourant().getSprinteur().setVitesse(vitesse);
						}
						if(!(game.getJoueurCourant().getRouleur().isFin())) {
							System.out.println(Jeu.effacerConsole());
							System.out.println("\nTuile actuel du Rouleur (vous �tes le joueur "+game.getJoueurCourant().getNumero()+"):\n"+game.getJoueurCourant().getSprinteur().getTuilePresent()+"\n\n");
							int vitesse = game.tirerPiocheRouleur(game.getJoueurCourant());
							System.out.println("\nTuile actuel du Rouleur :\n"+game.getJoueurCourant().getRouleur().getTuilePresent()+"\n\n");
							game.getJoueurCourant().getRouleur().setVitesse(vitesse);
						}	
					}
				}				
			}
			game.miseAjourClassement();
			System.out.println(Jeu.effacerConsole());
			System.out.println("__Phase_Mouvement__\n\n");
			for(int i = 0; i < game.getClassementJoueur().size();i++) {
				game.setJoueurCourant(game.getClassementJoueur().get(i));
				System.out.println("\nC'est a "+game.getJoueurCourant().getNom()+" de jouer ! (Joueur numero "+game.getJoueurCourant().getNumero()+")");
				if(!(game.getJoueurCourant().isArrivee())){
					if(!(game.getJoueurCourant().getRouleur().isFin())) {
						System.out.println("__Deplacement du Rouleur ! \n");
						game.getJoueurCourant().getRouleur().deplacement(game.getPlateau());
						System.out.println("\nTuile apres deplacement du Rouleur :\n"+game.getJoueurCourant().getRouleur().getTuilePresent());
					}
					if(!(game.getJoueurCourant().getSprinteur().isFin())) {
						System.out.println(Jeu.effacerConsole());
						System.out.println("__Deplacement du Sprinteur ! \n");
						game.getJoueurCourant().getSprinteur().deplacement(game.getPlateau());
						System.out.println("\nTuile apres deplacement du Sprinteur :\n"+game.getJoueurCourant().getRouleur().getTuilePresent());
					}

				}
			}
			game.miseAjourClassement();
			System.out.println("__Phase_Finale__\n\n");
			for(int i = 0; i < game.getDeroulement().size(); i++) {
				game.setJoueurCourant(game.getDeroulement().get(i));
				if(!(game.getJoueurCourant().isArrivee())){
					game.getJoueurCourant().getPioche().getPiRouleur().retirerCarteJouer(game.getJoueurCourant());
					game.getJoueurCourant().getPioche().getPiSprinteur().retirerCarteJouer(game.getJoueurCourant());;
				}
			}
			game.aspiration();
			System.out.println(Jeu.effacerConsole());
			System.out.println("\nAspiration effectue !\n");
			game.miseAjourClassement();
			game.attribuerCarteFatigue(game.getPlateau());
			System.out.println("\nAttribution des cartes fatigue effectu� !\n");
			fin=game.arriver();
			game.sauvegarder();
		}
		System.out.println(Jeu.effacerConsole());
		System.out.println("Felicitation, "+game.getGagnant().getNom()+" a gagne la partie !");
	}
}

