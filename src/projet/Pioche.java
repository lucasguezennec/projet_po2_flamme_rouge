package projet;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
/**
 * Creation de la classe Pioche qui cree les pioche du joueur
 *
 */
public class Pioche implements Serializable { // RASSEMBLEZ PIOCHESRPINTEUR ET PIOCHEROULEUR DANS PIOCHE ET CONSTRUIRE DEUX PIOCHES
	/**
	 * attribut PiocheRouleur qui definit la pioche pour le rouleur du joueur
	 */
	private PiocheRouleur piRouleur;
	/**
	 * attribut PiocheSprinteur qui definit la pioche pour le sprinteur du joueur
	 */
	private PiocheSprinteur piSprinteur;//PEUT ETRE FAIRE UNE SOUS CLASSE ABSTRACT
		
	/**
	 * Constructeur qui cree les deux pioches du joueur 
	 */
	public Pioche() {
		this.piRouleur = new PiocheRouleur();
		this.piSprinteur = new PiocheSprinteur();
	}
	
	/**
	 * methode qui vise a echanger la pioche et la defausse 
	 */
	public void swapPiocheDefausse() {
		Iterator<Carte> iterator = this.getPiRouleur().getPiocheRouleur().iterator();
		int i=0;
		int nbrCartePiocheRouleur = 0;
		int nbrCartePiocheSprinteur = 0;
		
		//Verification du nombre de carte presente dans la pioche rouleur
		for(int j=0;j<getPiRouleur().getPiocheRouleur().size();j++) {
			if(this.getPiRouleur().getPiocheRouleur().get(i).getNombreDansPioche() == 0) {
				nbrCartePiocheRouleur++;
			}
		}
		
		//Echange defausse/pioche Rouleur
		while(iterator.hasNext() || (nbrCartePiocheRouleur == getPiRouleur().getPiocheRouleur().size())) {
			this.getPiRouleur().getPiocheRouleur().iterator().next().setNombreDansDefausse(0);
			this.getPiRouleur().getPiocheRouleur().iterator().next().setNombreDansPioche(3);
			i++;
		}
		//Verification du nombre de carte presente dans la pioche sprinteur
		for(int j=0;j<getPiSprinteur().getPiocheSprinteur().size();j++) {
			if(this.getPiSprinteur().getPiocheSprinteur().get(i).getNombreDansPioche() == 0) {
				nbrCartePiocheSprinteur++;
			}
		}
		//Echange defausse/pioche Sprinteur
		i=0;
		while(iterator.hasNext() || (nbrCartePiocheSprinteur == getPiSprinteur().getPiocheSprinteur().size())) {
			this.getPiSprinteur().getPiocheSprinteur().iterator().next().setNombreDansDefausse(0);
			this.getPiSprinteur().getPiocheSprinteur().iterator().next().setNombreDansPioche(3);
			i++;
		}
		
		

	}

	/**
	 * methode getter qui recupere la pioche Rouleur du joueur
	 * @return la pioche Rouleur
	 */
	public PiocheRouleur getPiRouleur() {
		return piRouleur;
	}

	/**
	 * methode getter qui recupere la pioche Sprinteur du joueur
	 * @return la pioche Sprinteur
	 */
	public PiocheSprinteur getPiSprinteur() {
		return piSprinteur;
	}
}
