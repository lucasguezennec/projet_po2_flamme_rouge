package projet;

import java.io.Serializable;

/**
 * Creation de la classe Plateau qui cree le plateau de jeu
 */
public class Plateau implements Serializable {
	/**
	 * attribut Tuile representant une tuile du plateau 
	 */
	private Tuile[] tuile;
	/**
	 * Constructeur qui cree un plateau de jeu 
	 * @throws Projet_Exception
	 */
	public Plateau() throws Projet_Exception {
		int i = 0;
		this.tuile = new Tuile[21];
		for(i=0; i<4;i++) {
			this.tuile[i] = new Tuile(2,i);
		}
		this.tuile[4] = new Tuile(0,4);
		this.tuile[5]= new Tuile(2,5);
		this.tuile[6]= new Tuile(0,6); // 0 = gauche
		this.tuile[7]= new Tuile(0,7); // h
		this.tuile[8]= new Tuile(0,8); // i
		this.tuile[9]= new Tuile(1,9); // j
		this.tuile[10]= new Tuile(1,10); // k
		for(i = 11; i<14;i++) {
		 this.tuile[i]= new Tuile(2,i);
		}//o
		this.tuile[14]= new Tuile(1,14);
		this.tuile[15]= new Tuile(1,15);//p
		this.tuile[16]= new Tuile(0,16);//q
		this.tuile[17]= new Tuile(1,17);//r
		this.tuile[18]= new Tuile(1,18);//s
		this.tuile[19]= new Tuile(0,19);//t
		this.tuile[20]= new Tuile(2,20);//FINAL


	}
	/**
	 * methode setter qui initialise une tuile du plateau 
	 * @param tuile, tuile du plateau
	 */
	public void setTuile(Tuile[] tuile) {
		this.tuile = tuile;
	}
	/**
	 * methode getter qui recupere la tuile du plateau concernee
	 * @return la tuile du plateau concernee
	 */
	public Tuile[] getTuile() {
		return this.tuile;
	}
	
	
}
