package projet;

import java.io.Serializable;

/**
 * Creation de la classe Tuile qui cree les tuiles du plateau
 *
 */
public class Tuile implements Serializable {
	/**
	 * attribut Cycliste qui donne la position d'un cycliste sur une tuile
	 */
	private Cycliste[][] file; // [file][numeroCase]
	/**
	 * attribut entier permettant de savoir si la tuile est 
	 * une ligne droite ou un virage (a droite ou a gauche)
	 */
	private int typeTuile;
	/**
	 * attribut entier permettant de savoir ou l'on est sur le plateau
	 */
	private int positionSurPlateau;

	/**
	 * Constructeur qui cree une tuile en verifiant si c'est un virage ou une ligne droite
	 * et qui la place sur le plateau 
	 * @param n, type de tuile (si c'est un virage ou une ligne droite)
	 * @param pos, position de la tuile sur le plateau
	 * @throws Projet_Exception
	 */
	public Tuile(int n,int pos) throws Projet_Exception {
		this.typeTuile = n;
		this.positionSurPlateau = pos;
		if(n < 0 || n>2) {
			throw(new Projet_Exception("Numero de tuile non valide"));
		}
		switch(n) {
		case 0:
			this.file = new Cycliste[2][2];
			for(int i= 0; i<this.file.length;i++) {
				for(int j = 0; j<this.file[i].length;j++) {
					this.file[i][j]=null;
				}
			}
			break;
		case 1:
			this.file = new Cycliste[2][2];
			for(int i= 0; i<this.file.length;i++) {
				for(int j = 0; j<this.file[i].length;j++) {
					this.file[i][j]=null;
				}
			}
			break;
		case 2:
			this.file = new Cycliste[2][6];
			for(int i= 0; i<this.file.length;i++) {
				for(int j = 0; j<this.file[i].length;j++) {
					this.file[i][j]=null;
				}
			}
			break;
		default :
			System.out.println("Erreur, aucune tuile ne correspond");
			break;
		}

	}

	public String toString() {
		String f="";
		int compteur=1;
		int nbrCyc=0;
		boolean trouveCyc = false;
		int[][] coordCyc=null;
		boolean cyclistePresentSurCase=false;
		if((nbrCyc=this.presenceCycliste())>0) {
			coordCyc= this.coordCycliste(nbrCyc);
		}

		if(this.getTypeTuile()==2) {
			for(int k = 0; k<2;k++) {//BOUCLE POUR LE NOMBRE DE FILE (2)

				for(int i = 0; i<25;i++) {//LIGNE DE '='
					f=f+"=";
				}

				f=f+"\n";

				for(int i = 0; i<1;i++) {
					for(int j = 0; j<14;j++) {
						if(compteur!=1) {
							compteur=1;
							if(nbrCyc != 0) {
								for(int l = 0; l<coordCyc.length;l++) {
									if((coordCyc[l][0]==k) && ((coordCyc[l][1]==0 && j==1) || (coordCyc[l][1]==1 && j==3) ||
											(coordCyc[l][1]==2 && j==5) || (coordCyc[l][1]==3 && j==7) ||
											(coordCyc[l][1]==4 && j==9) || (coordCyc[l][1]==5 && j==11)) ) {
										if(i==0) {
											try {
												f=f+this.getFile()[coordCyc[l][0]][coordCyc[l][1]].toString();
											}catch(NullPointerException e) {
												f=f+"ERR";
											}
											
											cyclistePresentSurCase=true;
										}
									}
								}
							}

							if(!cyclistePresentSurCase) {
								f=f+"   ";
							}else {
								cyclistePresentSurCase=false;
							}
							//compteur++;
						}else {
							compteur=0;
							f=f+"|";
						}
					}
					f=f+"\n";
				}


				for(int i = 0; i<25;i++) {
					f=f+"=";
				}

				f=f+"\n";

			}
		}else if(this.getTypeTuile()==1) {//CAS VIRAGE DROITE
			boolean memeligneCyc=false;

			f=f+" __________________________\n|\n";
			if(nbrCyc != 0) {
				for(int j = 0; j < coordCyc.length;j++) {
					if(coordCyc[j][0]==0 && coordCyc[j][1]==1) {
						f=f+"|           "+this.getFile()[coordCyc[j][0]][coordCyc[j][1]].toString()+"\n"; 
						trouveCyc=true;
					}
				}
			}
			if(nbrCyc == 0 || !trouveCyc) {
				f=f+"|\n";
			}
			f=f+"|    ______________________\n|   |\n";
			if(nbrCyc != 0) {
				for(int j = 0; j < coordCyc.length;j++) {
					if(coordCyc[j][0]==1 && coordCyc[j][1]==1) {
						f=f+"|   |       "+this.getFile()[coordCyc[j][0]][coordCyc[j][1]].toString()+"\n"; 
						trouveCyc=true;
					}
				}
			}
			if(nbrCyc == 0 || !trouveCyc) {
				f=f+"|   |\n";
			}
			f=f+"|   |    __________________\n|   |   |\n";

			if(nbrCyc != 0) {
				for(int i = 0; i < coordCyc.length;i++) {
					for(int j = 0; j < coordCyc[i].length;j++) {
						if(coordCyc[i][1]== 0 && coordCyc[j][1]==0 && i != j && !memeligneCyc) {
							f=f+"|"+this.getFile()[coordCyc[i][0]][coordCyc[i][1]].toString()+"|"+this.getFile()[coordCyc[j][0]][coordCyc[j][1]].toString()+"|\n|   |   |\n";
							memeligneCyc=true;
						}
					}
				}
				if(!memeligneCyc) {
					for(int j = 0; j<coordCyc.length;j++) {
						if(coordCyc[j][1]==0 && coordCyc[j][0]==0) {
							trouveCyc=true;
							f=f+"|"+this.getFile()[coordCyc[j][0]][coordCyc[j][1]].toString()+"|   |\n|   |   |\n";
						}else if(coordCyc[j][1]==0 && coordCyc[j][0]==1) {
							f=f+"|   |"+this.getFile()[coordCyc[j][0]][coordCyc[j][1]].toString()+"|\n|   |   |\n";
						}
					}

				}
			}
			if(!trouveCyc || nbrCyc == 0) {
				f=f+"|   |   |\n|   |   |\n";
			}

		}else if(this.getTypeTuile()==0) {

			boolean memeligneCyc=false;

			f=f+" __________________________\n                           |\n";
			if(nbrCyc != 0) {
				for(int j = 0; j < coordCyc.length;j++) {
					if(coordCyc[j][0]==0 && coordCyc[j][1]==1) {
						f=f+"           "+this.getFile()[coordCyc[j][0]][coordCyc[j][1]].toString()+"           |\n"; 
						trouveCyc=true;
					}
				}
			}
			if(nbrCyc == 0 || !trouveCyc) {
				f=f+"                           |\n";
			}
			trouveCyc =false;
			f=f+" ______________________    |\n                       |   |\n";
			if(nbrCyc != 0) {
				for(int j = 0; j < coordCyc.length;j++) {
					if(coordCyc[j][0]==1 && coordCyc[j][1]==1) {
						f=f+"            "+this.getFile()[coordCyc[j][0]][coordCyc[j][1]].toString()+"        |   |\n"; 
						trouveCyc=true;
					}
				}
			}
			if(nbrCyc == 0 || !trouveCyc) {
				f=f+"                       |   |\n";
			}
			trouveCyc=false;
			f=f+" __________________    |   |\n                   |   |   |\n";
			if(nbrCyc != 0) {
				for(int i = 0; i < coordCyc.length;i++) {
					for(int j = 0; j < coordCyc[i].length;j++) {
						if(coordCyc[i][1]== 0 && coordCyc[j][1]==0 && i != j && !memeligneCyc) {
							f=f+"                   |"+this.getFile()[coordCyc[i][0]][coordCyc[i][1]].toString()+"|"+this.getFile()[coordCyc[j][0]][coordCyc[j][1]].toString()+"|\n                   |   |   |\n";
							memeligneCyc=true;
							trouveCyc=true;
						}
					}
				}
				if(!memeligneCyc) {
					for(int j = 0; j<coordCyc.length;j++) {
						if(coordCyc[j][1]==0 && coordCyc[j][0]==0) {
							trouveCyc=true;
							f=f+"                   |"+this.getFile()[coordCyc[j][0]][coordCyc[j][1]].toString()+"|   |\n                   |   |   |\n";
						}else if(coordCyc[j][1]==0 && coordCyc[j][0]==1) {
							f=f+"                   |   |"+this.getFile()[coordCyc[j][0]][coordCyc[j][1]].toString()+"|\n                   |   |   |\n";
						}
					}

				}
			}
			if(!trouveCyc || nbrCyc == 0) {
				f=f+"                   |   |   |\n                   |   |   |\n";
			}
		}
		return f;
	}

	/**
	 * methode permettant de savoir combien de cyclistes sont presents sur la tuile 
	 * @return le nombre de cyclistes presents sur la tuile 
	 */
	public int presenceCycliste() {
		int res=0;
		for(int i = 0; i<this.getFile().length;i++) {
			for(int j = 0; j<this.getFile()[i].length;j++) {
				if(this.getFile()[i][j] != null) {
					res++;
				}
			}
		}
		return res;
	}
	/**
	 * methode permettant de connaitre les coordonnees (file et case) des cyclistes
	 * presents sur la tuile
	 * @param nbrCy, nombre de cyclistes presents sur la tuile
	 * @return les coordonnees d'un cycliste sur une tuile 
	 */
	public int[][] coordCycliste(int nbrCy){
		int[][] res = new int[nbrCy][2];
		int indice=0;
		for(int i = 0; i<this.getFile().length;i++) {
			for(int j = 0; j<this.getFile()[i].length;j++) {
				if(this.getFile()[i][j] != null) {
					res[indice][0]= this.getFile()[i][j].getFileC();
					res[indice][1]= this.getFile()[i][j].getPos();
					indice++;
				}
			}
		}
		for(int i = 0; i < res.length;i++) {
				System.out.println("\n COORDCYC :"+res[i][1] + res[i][0] );
		}
		
		return res;
	}


	/**
	 * methode getter pour recuperer la position de la tuile sur le plateau
	 * @return la position de la tuile sur le plateau
	 */
	public int getPositionSurPlateau() {
		return positionSurPlateau;
	}

	/**
	 * methode setter pour initialiser la position de la tuile sur le plateau
	 * @param positionSurPlateau, position de la tuile sur le plateau
	 */
	public void setPositionSurPlateau(int positionSurPlateau) {
		this.positionSurPlateau = positionSurPlateau;
	}




	/**
	 * methode getter pour recuperer le cycliste sur la tuile
	 * @return le cycliste sur les coordonnees dans l'attribut file
	 */
	public Cycliste[][] getFile() {
		return file;
	}

	/**
	 * methode setter qui initialise un cycliste sur une tuile 
	 * @param file, position du cycliste sur la tuile 
	 */
	public void setFile(Cycliste[][] file) {
		this.file = file;
	}

	/**
	 * methode getter pour recuperer le type de la tuile
	 * @return l'entier correspondant a un type de tuile 
	 */
	public int getTypeTuile() {
		return typeTuile;
	}

	/**
	 * methode setter qui initialise le type de la tuile
	 * @param typeTuile, numero correspondant a un type de tuile 
	 */
	public void setTypeTuile(int typeTuile) {
		this.typeTuile = typeTuile;
	}

}
