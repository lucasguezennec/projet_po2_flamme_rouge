package projet;

import java.io.Serializable;

/**
 * Creation de la classe Carte qui definit une carte du jeu
 * Chaque carte sera differente de par sa valeur de vitesse
 */
public class Carte implements Serializable { // en faire une interface ou une abstract ou juste heriter
	/**
	 * attribut vitesse pour savoir le nombre de points 
	 * de vitesse attribue par la carte
	 */
	private int vitesse;
	/**
	 * attribut pour savoir le nombre de carte restantes dans la pioche
	 */
	private int NombreDansPioche;
	/**
	 * attribut pour savoir le nombre de carte dans la defausse
	 */
	private int NombreDansDefausse;
	/**
	 * Constructeur de la classe Carte qui construit une carte en fonction
	 * de la vitesse donnees en parametre 
	 * @param v vitesse donnee pour la carte
	 */
	public Carte(int v) {
		if(v == 2) {
			setNombreDansPioche(0);
			setNombreDansDefausse(0);
			setVitesse(v);
		}else {
			setNombreDansPioche(3);
			setNombreDansDefausse(0);
			setVitesse(v);
		}
	}

	public String toString() {
		String res="Carte-V"+this.vitesse+" ";
		return res;
	}
	
	
	
	
	
	/**
	 * methode getter pour recuperer la vitesse de la carte
	 */
	public int getVitesse() {
		return vitesse;
	}
	/**
	 * methode setter pour initialiser la vitesse de la carte
	 * @param vitesse vitesse donnee
	 */
	public void setVitesse(int vitesse) {
		this.vitesse = vitesse;
	}
	/**
	 * methode getter pour recuperer le nombre de carte de la pioche
	 */
	public int getNombreDansPioche() {
		return NombreDansPioche;
	}
	/**
	 * methode setter pour initialiser le nombre de carte dans la pioche
	 */
	public void setNombreDansPioche(int nombreDansPioche) {
		NombreDansPioche = nombreDansPioche;
	}
	/**
	 * methode getter pour recuperer le nombre de carte dans la defausse
	 */
	public int getNombreDansDefausse() {
		return NombreDansDefausse;
	}
	/**
	 * methode setter pour initialiser le nombre de carte dans la defausse
	 */
	public void setNombreDansDefausse(int nombreDansDefausse) {
		NombreDansDefausse = nombreDansDefausse;
	}

}
