package projet;

import java.io.Serializable;

/**
 * Creation de la classe Joueur qui cree un joueur 
 *
 */
public class Joueur implements Serializable {
	/**
	 * attributs String qui donne le nom du joueur et la couleur de ses pions 
	 */
	private String nom,couleur;
	/**
	 * attribut entier qui donne le numero du joueur 
	 */
	private int numero;
	/**
	 * attribut Rouleur qui assigne un rouleur au joueur
	 */
	private Rouleur rouleur;
	/**
	 * attribut Cycliste qui designe le pion a deplacer
	 */
	private Cycliste pionChoisit;
	/**
	 * attribut Sprinteur qui assigne un sprinteur au joueur
	 */
	private Sprinteur sprinteur;
	/**
	 * attribut Pioche qui assigne une pioche au joueur
	 */
	private Pioche pioche;
	/**
	 * attribut boolean qui indique si le joueur a fini la partie
	 */
	private boolean arrivee;
	/**
	 * Constructeur qui cree un joueur a partir de parametres donnes
	 * @param n, nom du joueur 
	 * @param c, couleur de ses pions 
	 * @param num, numero du joueur
	 * @param r, rouleur du joueur
	 * @param s, sprinteur du joueur
	 */
	public Joueur(String n, String c, int num, Rouleur r,Sprinteur s) {
		this.nom = n;
		this.couleur = c;
		this.numero = num;
		this.rouleur=r;
		this.sprinteur=s;
		this.pioche=new Pioche();
		this.arrivee = false;
		this.pionChoisit = null;
	}
	/**
	 * methode getter qui recupere le pion choisi par le joueur
	 * @return le pion que le joueur a choisit
	 */
	public Cycliste getPionChoisit() {
		return pionChoisit;
	}
	/**
	 * methode setter qui initialise le pion choisi par le joueur
	 * @param pionChoisit, pion choisi par le joueur
	 */
	public void setPionChoisit(Cycliste pionChoisit) {
		this.pionChoisit = pionChoisit;
	}
	/**
	 * methode qui regarde si un joueur est arrive
	 * @return vrai si le joueur est arrive ou faux s'il ne l'est pas
	 */
	public boolean isArrivee() {
		return arrivee;
	}
	/**
	 * methode setter qui initialise le booleen arrivee
	 */
	public void setArrivee(boolean arrivee) {
		this.arrivee = arrivee;
	}
	/**
	 * methode setter qui initalise la pioche du joueur
	 * @param pioche
	 */
	public void setPioche(Pioche pioche) {
		this.pioche = pioche;
	}
	/**
	 * m�thode getter qui r�cup�re la pioche du joueur
	 * @return la pioche du joueur  
	 */
	public Pioche getPioche() {
		return pioche;
	}
	/**
	 * Constructeur qui initialise un joueur
	 */
	public Joueur() {
		this.nom = null;
		this.numero = -1;
		this.rouleur = null;
		this.sprinteur = null;
		this.pioche = new Pioche();
	}
	/**
	 * methode getter qui recupere le nom du joueur 
	 * @return nom du joueur
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * methode setter qui initialise le nom du joueur avec celui donne en parametre
	 * @param nom, nom du joueur
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * methode getter qui recupere la couleur des pions du joueur 
	 * @return la couleur des pions
	 */
	public String getCouleur() {
		return couleur;
	}
	/**
	 * methode setter qui initialise la couleur avec celle passee en parametre
	 * @param couleur, couleur des pions 
	 */
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	/**
	 * methode getter qui recupere le num�ro du joueur 
	 * @return le numero du joueur
	 */
	public int getNumero() {
		return numero;
	}
	/**
	 * methode setter qui initialise le num�ro du joueur avec celui passe en parametre
	 * @param numero, numero du joueur
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}
	/**
	 * methode getter qui recupere le rouleur du joueur 
	 * @return le rouleur du joueur
	 */
	public Rouleur getRouleur() {
		return rouleur;
	}
	/**
	 * methode setter qui initialise le rouleur du joueur avec celui passe en parametre
	 * @param rouleur, rouleur du joueur
	 */
	public void setRouleur(Rouleur rouleur) {
		this.rouleur = rouleur;
	}
	/**
	 * methode getter qui recupere le sprinteur du joueur 
	 * @return le sprinteur du joueur
	 */
	public Sprinteur getSprinteur() {
		return sprinteur;
	}
	/**
	 * methode setter qui initialise le sprinteur du joueur avec celui passe en parametre
	 * @param sprinteur, sprinteur du joueur 
	 */
	public void setSprinteur(Sprinteur sprinteur) {
		this.sprinteur = sprinteur;
	}
	
	
	
}
