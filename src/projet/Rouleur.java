package projet;

import java.io.Serializable;
import java.util.Scanner;
/**
 * Creation de la classe Rouleur qui herite de la classe Cycliste qui cree un Rouleur
 */
public class Rouleur extends Cycliste implements Serializable{

	/**
	 * Constructeur qui cree un nouveau Rouleur a partir de parametres donnes 
	 * @param numJoueur, numero du joueur
	 * @param f, numero de la file du rouleur 
	 * @param pos, numero de la case ou est situe le Rouleur
	 * @param v, vitesse du rouleur
	 * @param tp, tuile sur laquelle est situe le rouleur
	 */
	public Rouleur(int numJoueur, int f,int pos,int v,Tuile tp) {
		super(numJoueur,f,pos,v,tp);
	}
	/**
	 * Constructeur qui cree un rouleur a partir d'un rouleur passe en parametre
	 * @param r, Rouleur 
	 */
	public Rouleur(Rouleur r) {
		super(r.getNumJoueur(),r.getFileC(),r.getPos(),r.getVitesse(),r.getTuilePresent());
	}
	/**
	 * Constructeur qui initialise un rouleur sur un tuile passee en parametre
	 * @param tp, tuile sur laquelle le rouleur est
	 */
	public Rouleur(Tuile tp) {
		super(0,0,0,0,tp);
	}
	/**
	 * methode qui fait deplacer le rouleur du nombre inscrit sur la carte tiree
	 * @param pl, plateau sur lequel evoluent les rouleurs
	 */
	public void deplacement(Plateau pl) throws Projet_Exception  { 
		int pointDeplacement = super.getVitesse();
		Scanner sc = new Scanner(System.in);
		int choix =0;
		int erreur=0;
		String test="";
		while(pointDeplacement != 0) {

			if(!this.avancerPossible(pl)) {
				System.out.println("\nAucun deplacement possible !");
				pointDeplacement=0;
			}else {
				do {
					erreur=1;
					System.out.println("Tuile avant deplacement : \n"+this.getTuilePresent());
					System.out.println("_Deplacement :\n1. Changer de file (0 point utilise)\n2. Avancer (1 point utilise)");
					choix=sc.nextInt();
					try {
						choix=Integer.parseInt(test);
						if(choix != 1 && choix != 2) {
							throw(new Projet_Exception("Entrez un nombre entre 1-2"));
						}
						if(this.detecterCyclisteDevant() && choix==2) {
							throw(new Projet_Exception("\nVous ne pouvez pas avancer car un cycliste est devant vous !\n"));
						}
					}catch(NumberFormatException e) {

						erreur=0;
					}
					catch(Projet_Exception e) {
						System.out.println(e.toString());
						erreur=1;
					}
				}while(erreur==1);
				switch(choix) {
				case 1://CHANGEMENT DE FILE
					this.changementFile(pl);
					break;
				case 2: //VERIFIER CYCLISTE DEVANT
					pointDeplacement--;
					this.avancer(pl);
					break;
				}
			}

			System.out.println(Jeu.effacerConsole());
		}
	}
	/**
	 * methode permettant de faire avancer le rouleur sur le plateau
	 * @param pl, plateau de jeu
	 */
	public void avancer(Plateau pl) {
		if(super.getPos() == 5 && super.getTuilePresent().getTypeTuile() ==2) {
			super.setPos(0);//Mise a jour de l'attribut a la premiere case de la prochaine tuile
			super.setTuilePresent(pl.getTuile()[super.getTuilePresent().getPositionSurPlateau()+1]);//Changement de tuile
			super.getTuilePresent().getFile()[super.getFileC()][0]=this.copierRouleur();// Copie du cycliste sur la case 
			pl.getTuile()[super.getTuilePresent().getPositionSurPlateau()-1].getFile()[super.getFileC()][5] =null; //Suppression de l'ancien cycliste
		}
		else if(super.getPos() == 1 && (super.getTuilePresent().getTypeTuile() == 0 || super.getTuilePresent().getTypeTuile() == 1)) {//Si fin de virage
			super.setPos(0);//MISE A JOUR ATTRIBUT
			super.setTuilePresent(pl.getTuile()[super.getTuilePresent().getPositionSurPlateau()+1]);//Changement de tuile
			super.getTuilePresent().getFile()[super.getFileC()][0]=this.copierRouleur();// Copie du cycliste sur la case 
			pl.getTuile()[super.getTuilePresent().getPositionSurPlateau()-1].getFile()[super.getFileC()][1] =null; //Suppression de l'ancien cycliste


		}
		else {//AVANCEMENT DE +1

			int position = super.getPos();
			super.setPos(position+1);
			super.getTuilePresent().getFile()[super.getFileC()][super.getPos()]= this.copierRouleur();//COPIE DU ROULEUR
			super.getTuilePresent().getFile()[super.getFileC()][super.getPos()-1]= null;//SUPPRESSION DU ROULEUR

		}

	}
	/**
	 * methode pour voir s'il ont peut avancer
	 */
	public boolean avancerPossible(Plateau pl) {
		return super.avancerPossible(pl);
	}
	/**
	 * methode pour detecter si il y a un cycliste devant 
	 */
	public boolean detecterCyclisteDevant() {
		return super.detecterCyclisteDevant();
	}
	/**
	 * methode permettant de faire changer de file le rouleur
	 * @param pl, plateau de jeu
	 */
	public void changementFile(Plateau pl) {

		if(super.getFileC() == 0) {//PASSAGE A DROITE
			super.setFileC(1);
			super.getTuilePresent().getFile()[1][super.getPos()]= this.copierRouleur();
			super.getTuilePresent().getFile()[0][super.getPos()]= null;
		}else {//PASSAGE A GAUCHE
			super.setFileC(0);
			super.getTuilePresent().getFile()[0][super.getPos()]= this.copierRouleur();
			super.getTuilePresent().getFile()[1][super.getPos()]= null;
		}

	}

	/**
	 * methode qui fait une copie du rouleur actuel
	 * @return le rouleur qui est copie
	 */
	public Rouleur copierRouleur() {
		Rouleur res = new Rouleur(this.getNumJoueur(), this.getFileC(), this.getPos(),this.getVitesse(),this.getTuilePresent());
		return res;
	}

	public String toString() {
		return (super.getNumJoueur()+"-R");
	}




}
