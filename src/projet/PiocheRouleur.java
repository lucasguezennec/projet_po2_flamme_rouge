package projet;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * Creation de la classe PiocheRouleur qui cree la pioche d'un joueur 
 * pour un rouleur
 *
 */
public class PiocheRouleur implements Serializable {
	/**
	 * attribut ArrayList qui est une liste de carte qui constitue la pioche pour le rouleur
	 */
	private ArrayList<Carte> piocheRouleur;
	/**
	 * Constructeur qui cree une pioche pour le rouleur
	 */
	public PiocheRouleur() {
		this.piocheRouleur = new ArrayList<Carte>();
		for(int i = 3; i<8;i++) {
			this.piocheRouleur.add(new Carte(i)); // Le contructeur de la classe Carte prend la vitesse en parametre
		}
		this.piocheRouleur.add(new Carte(2));
	}


	/**
	 * methode permettant de tirer une carte dans la pioche du rouleur
	 * @return les cartes tirees par le joueur
	 */
	public ArrayList<Carte> tirerCarte() {
		ArrayList<Carte> tirage = new ArrayList<Carte>();
		int compteurCarteRestante = 0;
		int sommeDesCartes=0;
		for(int i = 0; i<this.getPiocheRouleur().size();i++) {
			sommeDesCartes+=this.getPiocheRouleur().get(i).getNombreDansPioche();
		}
		for(int i = 0; i<this.getPiocheRouleur().size();i++) {
			if(this.getPiocheRouleur().get(i).getNombreDansPioche() > 0) {
				compteurCarteRestante++;
			}
		}
		if(compteurCarteRestante >= 4) {
			tirage=this.tirageAleatoireProba(sommeDesCartes);
		}else {
			for(int i = 0; i<this.getPiocheRouleur().size();i++) {
				if(this.getPiocheRouleur().get(i).getNombreDansPioche() != 0) {
					tirage.add(this.getPiocheRouleur().get(i));
				}
				this.swapPiocheDefausse();
				tirage.addAll(this.tirageAleatoireProba(sommeDesCartes));
			}
		}
		return tirage;
	}
	/**
	 * methode permettant de determiner la carte tiree selon leurs nombres
	 * @param sommeDesCartes, sommes des cartes d'un meme type
	 * @return les cartes tirees par le joueur 
	 */
	public ArrayList<Carte> tirageAleatoireProba(int sommeDesCartes){
		ArrayList<Carte> res = new ArrayList<Carte>();
		int trouve=0;
		int i = 0;
		int rdm = 0;
		while(trouve!=4) {
			rdm=(int)(Math.random() * this.getPiocheRouleur().size());
			if(this.getPiocheRouleur().get(rdm).getNombreDansPioche() != 0) {
				trouve++;
				res.add(this.getPiocheRouleur().get(rdm));
				this.getPiocheRouleur().get(rdm).setNombreDansPioche(this.getPiocheRouleur().get(rdm).getNombreDansPioche()-1);
				this.getPiocheRouleur().get(rdm).setNombreDansDefausse(this.getPiocheRouleur().get(rdm).getNombreDansDefausse()+1);
			}
			
		}
		return res;
	}
	
	/**
	 * methode permettant d'�changer la pioche du rouleur et sa defausse 
	 */
	public void swapPiocheDefausse() {
		for(int i = 0; i < this.getPiocheRouleur().size();i++) {
			this.getPiocheRouleur().get(i).setNombreDansPioche(this.getPiocheRouleur().get(i).getNombreDansDefausse());
			this.getPiocheRouleur().get(i).setNombreDansDefausse(0);
		}
	}
	/**
	 * methode getter qui recupere la pioche pour un rouleur
	 * @return la pioche du rouleur
	 */
	public ArrayList<Carte> getPiocheRouleur() {
		return piocheRouleur;
	}
	/**
	 * methode permettant de retirer une carte au joueur
	 * @param j, Joueur a qui retirer la carte
	 */
	public void retirerCarteJouer(Joueur j) {
		int vitesse = j.getRouleur().getVitesse();
		boolean trouve = false;
		int i = 0;
		while(!trouve && i < this.getPiocheRouleur().size()) {
			if(this.getPiocheRouleur().get(i).getVitesse() == vitesse) {
				this.getPiocheRouleur().get(i).setNombreDansDefausse(this.getPiocheRouleur().get(i).getNombreDansDefausse()-1);
				trouve=true;
			}
			i++;
		}
	}
}
